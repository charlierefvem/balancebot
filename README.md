# Balance Bot

This repository contains the files associated with a self balancing robot 
project. The project consists of three main parts: the MATLAB model of the
system, the micropython firmware, and the Eagle PCB files.

## Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes. See deployment for notes on
how to recreate this project yourself.

### Prerequisites

You will need MATLAB 2018b if you want to run the simulation provided for help
tuning the system based on the mass parameters of your balance bot.

You will also need a Nucleo L476 board running MicroPython 1.9.4 in order to
reuse the code available in this repository.

Finally, you will need Autodesk Eagle 9.3.1 if you want to work with the PCB
design files.

You may also want to install a git client compatible with BitBucket such as
[Sourcetree](https://www.sourcetreeapp.com/) in order to maintain a fork
of this repository.

### Installing

Either fork this repository or download a zip file of the contents in order to
modify and run the code. 

### Deployment

Firmware installation is easy. With the programmed Nucleo plugged into a
computer, drag all of the files in the Python directory onto the mounted flash
device.

## MATLAB(C) Simulation

### Files

The simulation uses the following files:

| Files                    | Type          | Description                                                       |
| ------------------------ | ------------- | ----------------------------------------------------------------- |
| BalanceBotMain.m         | Script        | Main simulation script that runs the Simulink model               |
| BalanceBotClosedLoop.slx | Model         | Model of the closed loop system with nonlinear plant model.       |
| BalanceBot.jpg           | Image         | Icon used for masked subsystem in Simulink                        |

## Python Firmware

### Files

| Files                    | Type          | Description                                                       |
| ------------------------ | ------------- | ----------------------------------------------------------------- |
| main.py                  | Script        | Main script for running balance bot closed loop controller        |
| bldc_hall.py             | Module        | Brushless motor and hall sensor driver                            |
| BNO055.py                | Module        | IMU driver                                                        |
| cotask.py                | Module        | Scheduler for multitasking                                        |
| print_task.py            | Module        | Printing task for serial comms                                    |
| task_share.py            | Module        | Driver for sharing data between tasks                             |
| mainpage.dox             | Documentation | Documentation for firmware                                        |
| Doxyfile                 | Configuration | Configuration file for doxygen                                    |
| images\by-nc-sa.svg      | Image         | License icon                                                      |

## Eagle PCB Design

### Files

| Files                    | Type          | Description                                                       |
| ------------------------ | ------------- | ----------------------------------------------------------------- |
| Dual_BLDC_Hat_AR.brd     | Board         | Nucleo "shoe" with dual BLDC motor drivers board layout           |
| Dual_BLDC_Hat_AR.sch     | Schematic     | Nucleo "shoe" with dual BLDC motor drivers circuit design         |
| Dual_BLDC_Hat_AR.csv     | Spreadsheet   | Bill of materials for Nucleo "shoe"                               |
| Pinout.xlsx              | Spreadsheet   | Pin configuration used in python firmware and schematic design    |
| oshpark_2layer.dru       | Design Rules  | Eagle design rules                                                |
| Dual_BLDC_Hat_AR...zip   | Gerbers       | Manufacturing files for revision 1 the Nucelo "shoe"              |

## Authors

* **Charlie Refvem** - *Initial work* - [crefvem@calpoly.edu](mailto:crefvem@calpoly.edu)

## License
![alt text](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-nc-sa.svg "by-nc-sa")

*Copyright (C) 2019 Charlie Refvem*

This document is copyright 2019 by C Refvem.
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.