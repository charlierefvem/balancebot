## @file receiver.py
#  This file contains the receiver module.
#  @package receiver
#  The receiver module allows the user to interact with radios such as
#  those used for RC cars or quadcopters.
import pyb
import micropython


##@brief Facilitates interaction with a RC radio receiver
#
# This class can accomodate PWM input from an RC-style radio receiver with
# 1 through 4 channels.
#
class RX:
    ## Creates instance of class RX.
    #
    # @param tim_num The number of the timer to use for counting. Make sure
    #                the selected timer has enough channels to accommodate
    #                the number of receiver channels
    # @param ch1 A constant from pyb.Pin indicated the desired pin for channel 1
    # @param ch2 A constant from pyb.Pin indicated the desired pin for channel 2
    # @param ch3 A constant from pyb.Pin indicated the desired pin for channel 3
    # @param ch4 A constant from pyb.Pin indicated the desired pin for channel 4
    def __init__(self, tim_num, ch1=None, ch2=None, ch3=None, ch4=None):
        ## The timer used for counting ticks for channels 1 through 4
        self.tim = pyb.Timer(tim_num, prescaler=79, period=0xffff)
        
        if ch1:
            ## The pin object associated with channel 1
            self.ch1 = pyb.Pin(ch1)
            self.tim.channel(1, mode=pyb.Timer.IC, callback=self.cb1,
                             pin=self.ch1, polarity=pyb.Timer.BOTH)
            ## The timer counter value at the current edge for channel 1
            self.cur_ticks1 = self.tim.counter()
            ## The timer counter value at the last edge for channel 1
            self.last_ticks1 = self.cur_ticks1
            ## The differnce in timer counter value between the last edge
            #  and the current edge for channel 1
            self.width1 = 0
            
        if ch2:
            ## The pin object associated with channel 2
            self.ch2 = pyb.Pin(ch2)
            self.tim.channel(2, mode=pyb.Timer.IC, callback=self.cb2,
                             pin=self.ch2, polarity=pyb.Timer.BOTH)
            ## The timer counter value at the current edge for channel 2
            self.cur_ticks2 = self.tim.counter()
            ## The timer counter value at the last edge for channel 2
            self.last_ticks2 = self.cur_ticks2
            ## The differnce in timer counter value between the last edge
            #  and the current edge for channel 2
            self.width2 = 0
            
        if ch3:
            ## The pin object associated with channel 3
            self.ch3 = pyb.Pin(ch3)
            self.tim.channel(3, mode=pyb.Timer.IC, callback=self.cb3,
                             pin=self.ch3, polarity=pyb.Timer.BOTH)
            ## The timer counter value at the current edge for channel 3
            self.cur_ticks3 = self.tim.counter()
            ## The timer counter value at the last edge for channel 3
            self.last_ticks3 = self.cur_ticks3
            ## The differnce in timer counter value between the last edge
            #  and the current edge for channel 3
            self.width3 = 0
            
        if ch4:
            ## The pin object associated with channel 4
            self.ch4 = pyb.Pin(ch4)
            self.tim.channel(4, mode=pyb.Timer.IC, callback=self.cb4,
                             pin=self.ch4, polarity=pyb.Timer.BOTH)
            ## The timer counter value at the current edge for channel 4
            self.cur_ticks4 = self.tim.counter()
            ## The timer counter value at the last edge for channel 4
            self.last_ticks4 = self.cur_ticks4
            ## The differnce in timer counter value between the last edge
            #  and the current edge for channel 4
            self.width4 = 0
    
    ## Input capture callback function associated with channel 1
    def cb1(self,tim):
        self.cur_ticks1 = tim.channel(1).capture()
        if not self.ch1.value():
            # Falling edge
            self.width1 = self.cur_ticks1 - self.last_ticks1
            if self.width1 < 0:
                self.width1 += 0x10000
        self.last_ticks1 = self.cur_ticks1
        
    ## Input capture callback function associated with channel 2
    def cb2(self,tim):
        self.cur_ticks2 = tim.channel(2).capture()
        if not self.ch2.value():
            # Falling edge
            self.width2 = self.cur_ticks2 - self.last_ticks2
            if self.width2 < 0:
                self.width2 += 0x10000
        self.last_ticks2 = self.cur_ticks2

    ## Input capture callback function associated with channel 3
    def cb3(self,tim):
        self.cur_ticks3 = tim.channel(3).capture()
        if not self.ch3.value():
            # Falling edge
            self.width3 = self.cur_ticks3 - self.last_ticks3
            if self.width3 < 0:
                self.width3 += 0x10000
        self.last_ticks3 = self.cur_ticks3

    ## Input capture callback function associated with channel 4
    def cb4(self,tim):
        self.cur_ticks4 = tim.channel(4).capture()
        if not self.ch4.value():
            # Falling edge
            self.width4 = self.cur_ticks4 - self.last_ticks4
            if self.width4 < 0:
                self.width4 += 0x10000
        self.last_ticks4 = self.cur_ticks4