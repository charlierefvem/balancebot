## @file main.py
#  This file contains the main control loop code for the balance bot.

import pyb
import utime
import micropython
from bldc_hall import BLDC_Hall
from encoder import QuadEncoder
from BNO055 import BNO055
from receiver import RX
from math import sin
from math import cos
from math import pi

pyb.repl_uart(None)
ser = pyb.UART(2,baudrate=1382400,timeout=1)


# Read A matrix
f = open("amatrix.csv","r")
lines = f.readlines()
A = []
for line in lines:
    L=line.strip().split()
    r=[]
    for col in L:
        r.append(float(col.strip(',')))
    A.append(r)
f.close()

# Read B matrix
f = open("bmatrix.csv","r")
lines = f.readlines()
B = []
for line in lines:
    L=line.strip().split()
    r=[]
    for col in L:
        r.append(float(col.strip(',')))
    B.append(r)
f.close()

# Read C matrix
f = open("cmatrix.csv","r")
lines = f.readlines()
C = []
for line in lines:
    L=line.strip().split()
    r=[]
    for col in L:
        r.append(float(col.strip(',')))
    C.append(r)
f.close()

# Read D matrix
f = open("dmatrix.csv","r")
lines = f.readlines()
D = []
for line in lines:
    L=line.strip().split()
    r=[]
    for col in L:
        r.append(float(col.strip(',')))
    D.append(r)
f.close()


# Object used to interface with the IMU
i2c = pyb.I2C(2,pyb.I2C.MASTER)
imu = BNO055(i2c)
imu.enable_config()
utime.sleep_ms(30)
imu.write_calib_bytes()
utime.sleep_ms(30)
imu.enable_ndof()


# Allocate memory for exceptions within interrupts
micropython.alloc_emergency_exception_buf(100)


# Object used to interact with the radio receiver
rx = RX(15,ch1=pyb.Pin.cpu.B14,ch2=pyb.Pin.cpu.B15)


# Objects used for interfacing with hall sensors and BLDC motors 1 and 2
M1 = BLDC_Hall (3,
                pyb.Pin.cpu.A6,
                pyb.Pin.cpu.A7,
                pyb.Pin.cpu.B0,
                1,
                pyb.Pin.cpu.A8,
                pyb.Pin.cpu.A9,
                pyb.Pin.cpu.A10,
                pyb.Pin.cpu.C9,
                pyb.Pin.cpu.C10,
                pyb.Pin.cpu.C11,
                pyb.Pin.cpu.B1,
                pyb.Pin.cpu.B12,
                pyb.Pin.cpu.B4)
                
M2 = BLDC_Hall (4,
                pyb.Pin.cpu.B6,
                pyb.Pin.cpu.B7,
                pyb.Pin.cpu.B8,
                8,
                pyb.Pin.cpu.C6,
                pyb.Pin.cpu.C7,
                pyb.Pin.cpu.C8,
                pyb.Pin.cpu.C3,
                pyb.Pin.cpu.C4,
                pyb.Pin.cpu.C5,
                pyb.Pin.cpu.B2,
                pyb.Pin.cpu.B13,
                pyb.Pin.cpu.B5)

# Objects used for interfacing with encoders 1 and 2
E1 = QuadEncoder (pyb.Pin.cpu.A15,
                  pyb.Pin.cpu.B3,
                  pyb.Timer(2),
                  2048,
                  dir=QuadEncoder.CW)

E2 = QuadEncoder (pyb.Pin.cpu.A0,
                  pyb.Pin.cpu.A1,
                  pyb.Timer(5),
                  2048,
                  dir=QuadEncoder.CW)

# Initialize motors and make sure they start up with no fault condition
M1.enable()
M2.enable()
utime.sleep(1)
M1.clear_fault()
M2.clear_fault()

# Print flag indicates if we are in data collection mode or not
prnt = True
skip_cal = True

rw = 45e-3                                  # <- Radius of the wheel
ppr = 16384                                 # <- Encoder pulses per revolution
usec2sec = 1e-6                             # <- Conversion from us -> s
ticks_period = 25000
Ts = ticks_period*usec2sec                  # <- Sample Time

# Measurement gains for each state
h1 = 2*pi/ppr                               # <- motor angle
h2 = -2*pi/360                              # <- pitch angle
h3 = 2*pi/360                               # <- yaw angle
h4 = h1/Ts                                  # <- motor speed
h5 = -h2                                    # <- pitch rate
h6 = h3                                     # <- yaw rate

# Feedback gains for each state
# 10/25/19 2:30PM - Slow oscillation in x1 and x4
# k1 =  0.0005                                # <- motor angle
# k2 =  0.1250                                # <- pitch angle
# k3 =  0.0010                                # <- yaw angle
# k4 =  0.0005                                # <- motor speed
# k5 =  0.0100                                # <- pitch rate
# k6 =  0.0010                                # <- yaw rate

# 10/25/19 2:45PM - Smaller amplitude but faster oscillation in x1 and x4
#                   better than previous gains
# k1 =  0.0010                                # <- motor angle
# k2 =  0.1250                                # <- pitch angle
# k3 =  0.0010                                # <- yaw angle
# k4 =  0.0010                                # <- motor speed
# k5 =  0.0100                                # <- pitch rate
# k6 =  0.0010                                # <- yaw rate

# 10/25/19 3:00PM - Much worse than the last run. Maybe k4 should be bigger than
#                   k1?
# k1 =  0.0015                                # <- motor angle
# k2 =  0.1250                                # <- pitch angle
# k3 =  0.0010                                # <- yaw angle
# k4 =  0.0010                                # <- motor speed
# k5 =  0.0100                                # <- pitch rate
# k6 =  0.0010                                # <- yaw rate

# 10/25/19 3:15PM - Much worse than the last run. k4, k1 too high
# k1 =  0.0010                                # <- motor angle
# k2 =  0.1250                                # <- pitch angle
# k3 =  0.0010                                # <- yaw angle
# k4 =  0.0015                                # <- motor speed
# k5 =  0.0100                                # <- pitch rate
# k6 =  0.0010                                # <- yaw rate


# 10/25/19 3:30PM - Best yet, k2 has been reduced
# k1 =  0.0010                                # <- motor angle
# k2 =  0.1100                                # <- pitch angle
# k3 =  0.0010                                # <- yaw angle
# k4 =  0.0010                                # <- motor speed
# k5 =  0.0100                                # <- pitch rate
# k6 =  0.0010                                # <- yaw rate

# Gains as of 11/28 testing - work OK but x/xd oscillate
# k1 =  0.0012                                # <- motor angle
# k2 =  0.1000                                # <- pitch angle
# k3 =  0.0010                                # <- yaw angle
# k4 =  0.0012                                # <- motor speed
# k5 =  0.0100                                # <- pitch rate
# k6 =  0.0010                                # <- yaw rate


k1 =  0.0010                                # <- motor angle
k2 =  0.1000                                # <- pitch angle
k3 =  0.0008                                # <- yaw angle
k4 =  0.0017                                # <- motor speed
k5 =  0.0110                                # <- pitch rate
k6 =  0.0008                                # <- yaw rate

# Initial setpoints for each state
x1_des = 0                                  # <- motor angle
x2_des = 0                                  # <- pitch angle
x3_des = 0                                  # <- yaw angle
x4_des = 0                                  # <- motor speed
x5_des = 0                                  # <- pitch rate
x6_des = 0                                  # <- yaw rate

# Raw input from remote
x4_raw = 0                                  # <- motor speed
x6_raw = 0                                  # <- yaw rate

# Initial values for each state
x1 = 0                                      # <- motor angle
x2 = 0                                      # <- pitch angle
x3 = 0                                      # <- yaw angle
x4 = 0                                      # <- motor speed
x5 = 0                                      # <- pitch rate
x6 = 0                                      # <- yaw rate

# Gain for throttle and steering input
# kthr = 40/500                               # implies max wheel speed = 40 rad/s
# kstr = 15/500                               # implies max yaw rate = 15 rad/s
kthr = 40/500                               # implies max wheel speed = 20 rad/s
kstr = 20/500                               # implies max yaw rate = 10 rad/s

# Deadband for throttle and steering input
dbthr = 1                                   # Any command less than 1 rad/s
dbstr = 1                                   # Any command less than 1 rad/s

x2_max = 3*pi/8
x2_min = -3*pi/8

# Actuator gain
kpwm = 1/10.62*141.6*8*2000/12


# Saturation limit
sat_lim = 2000

ud1 = 0
ud2 = 0

xd1 = 0
xd2 = 0
xd3 = 0
xd4 = 0

yd1 = 0
yd2 = 0
yd3 = 0
yd4 = 0
yd5 = 0
yd6 = 0


# Calibration loop prints IMU calibration status and waits until the IMU
# is fully calibrated or the remote is used to abort the calibration procedure
if prnt == False and skip_cal == False:
    while True:
        cal = imu.get_calib()
        if cal[0]==3 and cal[1]==3 and cal[2]==3 and cal[3]==3:
            print('IMU CALIBRATED')
            break
        elif rx.width2 < 1100:
            print('IMU CALIBRATION ABORTED')
            break
        print('rx1 = {:}'.format(rx.width1))
        print('rx2 = {:}'.format(rx.width2))
        print('mag = {:}'.format(cal[0]))
        print('acc = {:}'.format(cal[1]))
        print('gyr = {:}'.format(cal[2]))
        print('sys = {:}\n'.format(cal[3]))
        utime.sleep_ms(500)


# Arming state. User must pull the trigger for full throttle then full break to
# arm the system and start the control loop
print('Ready to Arm')
while rx.width1 < 1700:
    pass
while rx.width1 > 1300:
    pass
print('Armed')

utime.sleep_ms(500)

str = ( 'Time,'
        'Throttle,'
        'Steering,'
        'x1_des,'
        'x1,'
        'x2_des,'
        'x2,'
        'x3_des,'
        'x3,'
        'x4_des,'
        'x4,'
        'x5_des,'
        'x5,'
        'x6_des,'
        'x6,'
        'M1 Duty,'
        'M2 Duty,'
        'checksum,'
        '\r\n')

ser.write(str)

# Initialize counters to perform proper timing in control loop
cur_ticks = utime.ticks_us()
next_ticks = utime.ticks_add(cur_ticks,ticks_period)
start_ticks = next_ticks


# Initialize heading
last_heading = imu.get_heading()
heading = 0


# Control loop measures from sensors, computes actuation signal, and outputs to
# the motors.
while True:
    cur_ticks = utime.ticks_us()
    if  cur_ticks >= next_ticks :
        
        # Check for a fault state on the motors and clear the fault if there is
        # one present. This is probably bad practice but is needed for now
        m1_flt = M1.get_fault_status()
        m2_flt = M2.get_fault_status()
        if m1_flt:
            # M1.set_duty(0)
            # M2.set_duty(0)
            # raise Exception('Motor 1 Fault')
            M1.clear_fault()
        if m2_flt:
            # M1.set_duty(0)
            # M2.set_duty(0)
            # raise Exception('Motor 2 Fault')
            M2.clear_fault()
    
        # Update the encoder readings
        E1.update()
        E2.update()
        
        # Get the raw data from each motor and encoder
        m1_th  = E1.get_position()
        m1_thd = E1.get_delta()
        m2_th  = E2.get_position()
        m2_thd = E2.get_delta()
        
        # Get and then "unwrap" the IMU data
        raw_heading = -imu.get_heading()
        delta_heading = raw_heading - last_heading
        if delta_heading > 180:
            delta_heading -= 360
        elif delta_heading < -180:
            delta_heading += 360
        heading += delta_heading
        last_heading = raw_heading
        
        # Convert the raw data from the sensors into the defined states
        x1 = h1*(m1_th-m2_th)
        x2 = h2*imu.get_pitch()
        x3 = h3*heading
        x4 = h4*(m1_thd-m2_thd)
        x5 = h5*imu.get_pitch_rate()
        x6 = h6*(imu.get_roll_rate()*sin(x2) + imu.get_yaw_rate()*cos(x2))
        
            
        
        # If the throttle input (radio channel 1) is in the proper range, use
        # data to manipulate the motor position and velocity setpoints
        if rx.width1 >=1350:
            # Throttle Setpoint
            x4_raw = (rx.width1-1500)*kthr
            if abs(x4_raw) < 0.5:
                x4_raw = 0
                
            # Steering setpoint
            x6_raw = (rx.width2-1500)*kstr
            if abs(x6_raw) < 0.5:
                x6_raw = 0
        else:
            x4_raw = 0
            x6_raw = 0

        ud1 = x4_raw
        ud2 = x6_raw
        
        xd1 = A[0][0]*xd1 + A[0][1]*xd2 + A[0][2]*xd3 + A[0][3]*xd4 + B[0][0]*ud1 + B[0][1]*ud2
        xd2 = A[1][0]*xd1 + A[1][1]*xd2 + A[1][2]*xd3 + A[1][3]*xd4 + B[1][0]*ud1 + B[1][1]*ud2
        xd3 = A[2][0]*xd1 + A[2][1]*xd2 + A[2][2]*xd3 + A[2][3]*xd4 + B[2][0]*ud1 + B[2][1]*ud2
        xd4 = A[3][0]*xd1 + A[3][1]*xd2 + A[3][2]*xd3 + A[3][3]*xd4 + B[3][0]*ud1 + B[3][1]*ud2
        
        yd1 = C[0][0]*xd1 + C[0][1]*xd2 + C[0][2]*xd3 + C[0][3]*xd4 + D[0][0]*ud1 + D[0][1]*ud2
        yd2 = C[1][0]*xd1 + C[1][1]*xd2 + C[1][2]*xd3 + C[1][3]*xd4 + D[1][0]*ud1 + D[1][1]*ud2
        yd3 = C[2][0]*xd1 + C[2][1]*xd2 + C[2][2]*xd3 + C[2][3]*xd4 + D[2][0]*ud1 + D[2][1]*ud2
        yd4 = C[3][0]*xd1 + C[3][1]*xd2 + C[3][2]*xd3 + C[3][3]*xd4 + D[3][0]*ud1 + D[3][1]*ud2
        yd5 = C[4][0]*xd1 + C[4][1]*xd2 + C[4][2]*xd3 + C[4][3]*xd4 + D[4][0]*ud1 + D[4][1]*ud2
        yd6 = C[5][0]*xd1 + C[5][1]*xd2 + C[5][2]*xd3 + C[5][3]*xd4 + D[5][0]*ud1 + D[5][1]*ud2


        x1_des = yd1 # x
        x2_des = yd2 # theta
        x3_des = yd3 # psi
        x4_des = yd4 # x dot
        x5_des = yd5 # theta dot
        x6_des = yd6 # psi dot

        # # Prefilter input from throttle
        # x1_des = x1_des + 0.0118*x4_des + 0.0007*x4_raw # x
        # x2_des =        - 0.1452*x4_des + 0.1452*x4_raw # theta
        # x3_des = x3_des + 0.0095*x6_des + 0.0005*x6_raw # psi
        # x4_des =          0.9048*x4_des + 0.0952*x4_raw # x dot
        # x5_des =        + 1.4521*x4_des - 1.4521*x4_raw # theta dot
        # x6_des =          0.9048*x6_des + 0.0952*x6_raw # psi dot
        
        if x2_des > pi/4:
            x2_des = pi/4
        elif x2_des < -pi/4:
            x2_des = -pi/4
        
        # If the bot has fallen over too far in either direction or the halt
        # signal comes from the remote, check if the system is estop or in
        # reset.
        #
        # Otherwise, compute the duty cycle using the state feedback gain and
        # actuator gain.
        if x2 > x2_max or x2 < x2_min or rx.width1 < 1300:
            duty1 = 0
            duty2 = 0
            # If in reset set both encoders and the position setpoint back to
            # zero
            if rx.width2 < 1100:
                E1.zero()
                E2.zero()
                x1_des = 0
                x3_des = 0
            # If in estop set both motors to zero duty cycle and exit the
            # control loop
            elif rx.width2 > 1900:
                M1.set_duty(0)
                M2.set_duty(0)
                break
        else:
            # duty = -kpwm*(k1*x1 + k2*x2 + k4*x4 + k5*x5)
            duty1 = -kpwm*(-k1*(x1-x1_des) - k2*(x2-x2_des) + k3*(x3-x3_des) - k4*(x4-x4_des) - k5*(x5-x5_des) + k6*(x6-x6_des))
            duty2 = -kpwm*(+k1*(x1-x1_des) + k2*(x2-x2_des) + k3*(x3-x3_des) + k4*(x4-x4_des) + k5*(x5-x5_des) + k6*(x6-x6_des))
        
        
        # Check for saturation in the duty cycle
        if duty1 > sat_lim:
            duty1 = sat_lim
        elif duty1 < -sat_lim:
            duty1 = -sat_lim
            
        if duty2 > sat_lim:
            duty2 = sat_lim
        elif duty2 < -sat_lim:
            duty2 = -sat_lim
        
        # Send the duty cycle out to the motors.
        M1.set_duty(duty1)
        M2.set_duty(duty2)
        
        
        # If the print flag is true send all the states and setpoints to the
        # REPL for debugging
        if prnt == True:
            # print('TIME = {:}'.format(cur_ticks))
            # print('M1 TH = {:}'.format(m1_th))
            # print('M1 THD = {:}'.format(m1_thd))
            # print('M1 DTY = {:}'.format(m1_duty))
            # print('M1 DTY* = {:}'.format(M1._duty))
            # print('M1 SEC = {:}'.format(M1.get_sector()))
            # print('M2 TH = {:}'.format(m2_th))
            # print('M2 THD = {:}'.format(m2_thd))
            # print('M2 DTY = {:}'.format(m2_duty))
            # print('M2 DTY* = {:}'.format(M2._duty))
            # print('M2 SEC = {:}'.format(M2.get_sector()))
            # print('TH = {:}'.format(x2))
            # print('THD = {:}\n'.format(x5))
            
            # print('x1d  = {:}'.format(x1_des))
            # print('x1   = {:}'.format(x1))
            # print('x2d  = {:}'.format(x2_des))
            # print('x2   = {:}'.format(x2))
            # print('x3d  = {:}'.format(x3_des))
            # print('x3   = {:}'.format(x3))
            # print('x4d  = {:}'.format(x4_des))
            # print('x4   = {:}'.format(x4))
            # print('x5d  = {:}'.format(x5_des))
            # print('x5   = {:}'.format(x5))
            # print('x6d  = {:}'.format(x6_des))
            # print('x6   = {:}'.format(x6))
            # print('dty1 = {:}'.format(duty1))
            # print('dty2 = {:}\n'.format(duty2))
            
            str = ( '{:},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g},'
                    '{:03.3g}').format( utime.ticks_diff(cur_ticks,start_ticks),
                                    ud1,
                                    ud2,
                                    x1_des,
                                    x1,
                                    x2_des,
                                    x2,
                                    x3_des,
                                    x3,
                                    x4_des,
                                    x4,
                                    x5_des,
                                    x5,
                                    x6_des,
                                    x6,
                                    duty1,
                                    duty2)
            chk = 0
            for char in str:
                chk += ord(char)
            chk &= 0xFF
            
            str = str + ',{:0.0}\r\n'.format(chk)
            
            ser.write(str)
            
            # ser.write('t    = {:}\r\n'.format(cur_ticks))
            # ser.write('x1d  = {:}\r\n'.format(x1_des))
            # ser.write('x1   = {:}\r\n'.format(x1))
            # ser.write('x2d  = {:}\r\n'.format(x2_des))
            # ser.write('x2   = {:}\r\n'.format(x2))
            # ser.write('x3d  = {:}\r\n'.format(x3_des))
            # ser.write('x3   = {:}\r\n'.format(x3))
            # ser.write('x4d  = {:}\r\n'.format(x4_des))
            # ser.write('x4   = {:}\r\n'.format(x4))
            # ser.write('x5d  = {:}\r\n'.format(x5_des))
            # ser.write('x5   = {:}\r\n'.format(x5))
            # ser.write('x6d  = {:}\r\n'.format(x6_des))
            # ser.write('x6   = {:}\r\n'.format(x6))
            # ser.write('dty1 = {:}\r\n'.format(duty1))
            # ser.write('dty2 = {:}\r\n\r\n'.format(duty2))
            
        next_ticks = utime.ticks_add(next_ticks, ticks_period)