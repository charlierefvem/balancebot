## @file encoder.py
#  This file contains the encoder module.
#  @package encoder
#  The encoder module allows the user to interact with encoders such as
#  incremental quadrature encoders.
import pyb
import micropython


##@brief The encoder class facilitates interaction with a quadrature encoder.
#
# The quadrature encoder must be wired so that the A and B channels
# connect to channels 1 and 2 of a timer.
class QuadEncoder:
    ## Counter clockwise
    CCW = micropython.const(0)
    ## Clockwise
    CW = micropython.const(1)


    ## @brief Creates a quadrature encoder object
    #  @param CHA A pyb.Pin object for channel A
    #  @param CHB A pyb.Pin object for channel B
    #  @param TIM The timer associated with CHA and CHB to be used for
    #             counting
    #  @param CPR The number of encoder cycles per revolution
    #  @param dir The direction associated with positive rotation.
    #             Defaults to clockwise.
    #             Should be one of the following:\n
    #                QuadEncoder.CCW\n
    #                QuadEncoder.CW
    def __init__(self,CHA,CHB,TIM,CPR,dir=CW):
        ## The pin used for channel A
        self._CHA = CHA
        ## The pin used for channel B
        self._CHB = CHB
        ## The timer used to count ticks
        self._TIM = TIM
        ## The period of the timer, equal to the total number of edges from 
        #  the encoder in one cycle. PER = 4 CPR - 1
        self._PER = 4*CPR-1
        ## The maximal encoder change between updates. Equal to half of the
        #  period. This prevents the delta from overflowing.
        self._DELTAMAX = int(self._PER/2)
        ## The minimal encoder change between updates. Equal to half of the
        #  period. This prevents the delta from overflowing
        self._DELTAMIN = self._DELTAMAX - self._PER
        self._TIM.init(prescaler=0,period=self._PER)
        self._TIM.channel(1,pin=self._CHA,mode=pyb.Timer.ENC_AB)
        self._TIM.channel(2,pin=self._CHB,mode=pyb.Timer.ENC_AB)
        ## The position of the encoder. Equal to the total number of encoder
        #  ticks since the encoder was initialized or zeroed.
        self._position = 0
        ## The number of encoder ticks in this revolution of the encoder.
        #  Can be used to determine the angle of the encoder.
        self._last_count = self._TIM.counter()
        ## The change in encoder ticks since the last update.
        self._delta = 0
        ## The current count of the timer.
        self._count = 0
        ## The most recent value of Encoder._delta before it was updated.
        self._last_delta = 0;
        ## Motor direction
        self._dir = dir
        
    
    ## @brief Updates the current position of the encoder
    #
    #  This function must be called at a regular interval in order to 
    #  avoid missing an overflow on the counter. The interval can be
    #  determined from the maximum angular velocity of the encoder
    #  using the following equation:
    #
    #  f_update = 2 f_encoder
    #
    #  where f_encoder is measured in revolutions per second
    # (Hz) and can be computed from the max rotational speed of the encoder. 
    def update(self,IRQsource=None):
        self._last_count = self._count
        self._last_delta = self._delta
        self._count = self._TIM.counter()
        self._delta = self._count - self._last_count
        
        if (self._delta>self._DELTAMAX):
            self._delta = self._delta - self._PER
        elif (self._delta<self._DELTAMIN):
            self._delta = self._delta + self._PER
        
        if (self._dir == self.CCW):
            self._delta = - self._delta
        
        self._position = self._position + self._delta
     
    ## @brief Resets the position of the encoder back to zero.
    def zero(self):
        self._last_count = self._TIM.counter()
        self._position = 0
    
    ## @brief Queries the position of the encoder
    #
    #  This method accounts for the rollover of the encoder counter and
    #  returns a signed number indicating the number of encoder ticks since
    #  the encoder was initialized or was zeroed.
    #  @return A signed integer representing the encoder position
    def get_position(self):
        return self._position
    
    ## @brief Queries the count of the encoder
    #
    #  This method returns the current encoder tick value. It can be used
    #  to determine the angle of the encoder. It does not account for rollover.
    #  @return An unsigned integer representing the encoder angle
    def get_count(self):
        return self._last_count
    
    ## @brief Queries the change in position of the encoder
    #
    #  If configured for callbacks, this represents the number of ticks over 
    #  the last 1ms period. If update is called manually, this represents the
    #  number of callbacks since the second most recent call of the
    #  Encoder.update() method. If the amount of
    #  time between calls to update() is known then this value may be used
    #  to estimate the speed of the encoder.
    #  @return A signed integer representing the change in encoder ticks
    #  between the last two updates.
    def get_delta(self):
        return (self._delta+self._last_delta)/2
    
    ## @brief Configure the encoder to use an additional timer to generate
    #         callbacks to update the motor periodically.
    #
    #  The callbacks will occur at a constant interval of 1ms. The callback
    #  will run the Encoder.update() method.
    def use_callback(self, TIM):
        TIM.init(freq=1000)
        TIM.callback(self.update)