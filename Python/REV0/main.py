import pyb
import micropython
import utime
from bldc_hall import BLDC_Hall
import BNO055
import cotask
import print_task
import task_share


i2c = pyb.I2C(2, pyb.I2C.MASTER)
imu = BNO055.BNO055(i2c)


micropython.alloc_emergency_exception_buf (100)


## Object used for interfacing with hall sensors and BLDC motor
M1 = BLDC_Hall (2,
                pyb.Pin.cpu.A0,
                pyb.Pin.cpu.A1,
                pyb.Pin.cpu.A2,
                1,
                pyb.Pin.cpu.A8,
                pyb.Pin.cpu.A9,
                pyb.Pin.cpu.A10,
                pyb.Pin.cpu.B13,
                pyb.Pin.cpu.B14,
                pyb.Pin.cpu.B15,
                pyb.Pin.cpu.C13,
                pyb.Pin.cpu.B2,
                pyb.Pin.cpu.C14)

M2 = BLDC_Hall (4,
                pyb.Pin.cpu.B6,
                pyb.Pin.cpu.B7,
                pyb.Pin.cpu.B8,
                8,
                pyb.Pin.cpu.C6,
                pyb.Pin.cpu.C7,
                pyb.Pin.cpu.C8,
                pyb.Pin.cpu.A5,
                pyb.Pin.cpu.B0,
                pyb.Pin.cpu.B1,
                pyb.Pin.cpu.C13,
                pyb.Pin.cpu.B3,
                pyb.Pin.cpu.C15)
                

M1.enable()
M2.enable()
utime.sleep(1)
M1.clear_fault()
M2.clear_fault()
# imu.enable_imu()
imu.enable_ndof()

## Actuate the motor to a desired setpoint with a PD controller
def step(setpoint=2000, k1=20, k2=0.5):
    global M1
    M1.set_position(0)
    runs = 0
    for n in range(1000):
        t=utime.ticks_ms()
        
        speed = M1.get_speed()
        position = M1.get_position()
        
        duty = int(k1*(setpoint - position)+k2*(-speed))
        M1.set_duty(duty)
        
        if (runs%100):
            print('{:},{:},{:},{:}'.format(t,position,speed,duty))
        
        runs = runs+1
    M1.set_duty(0)

## Read the IMU in a loop and print the results
def test_imu():
    print('---------------')
    print('gx,gy,gz,wx,wy,wz')
    for n in range(1000):
        gyro = imu.get_gyro()
        quat = imu.get_quat()
        omega = imu.rotate_vector(quat,gyro)
        print('{:},{:},{:},{:},{:},{:}'.format(gyro[0],gyro[1],gyro[2],omega[0],omega[1],omega[2]))
        utime.sleep_ms(10)
    print('---------------')
    
CALIBRATING = micropython.const (0)
GOING = micropython.const (1)

# LQR Gains
# k1 = 6
# k2 = 3
# k3 = 0
# k4 = 40

# LQRD Gains 250 Hz
k1 = -0.6293
k2 = -0.4327
k3 = -0.3820
k4 = -1.8718

# LQRD Gains 100 Hz
# k1 = -0.2504
# k2 = -0.1746
# k3 = -0.1507
# k4 = -0.8975

k1 = k4/5
k2 = k2/2
k3 = k3/5
k4 = k4

# Filter coefficients
alpha1 = 0.00       # xd
alpha2 = 0.00       # thd
alpha3 = 0.00       # x
alpha4 = 0.00       # th
alpha5 = 0.00       # dty

th_offset = 0

## Function which runs for Task 1, which performs full state feedback on the
#  balance bot.
def task1_fun ():
    global k1, k2, k3, k4
    global ks1, ks2, ks3, ks4
    global th_offset

    state = CALIBRATING
    counter = 0
    M1.set_position(0)
    runs = 0
    xr = 0


    # k1 = 1
    # k2 = 1
    # k3 = 1
    # k4 = 10

    xd_filt = 0
    thd_filt = 0
    x_filt = 0
    th_filt = 0
    
    deg2rad = (2*3.1415)/360
    sec2rad = (2*3.1415)/24
    sec2m  = sec2rad*0.045
    
    duty_filt = 0
    print('IMU offset computed as: theta={:}\n'.format(th_offset))
    print('Gains computed as: k=[{:}\t{:}\t{:}\t{:}]\n'.format(ks1,ks2,ks3,ks4))
    
    
    selftest = imu.get_selftest()
    print('Sensor self test:\n')
    print('MAG={:}\tACC={:}\tGYR={:}\tMCU={:}\n'.format(selftest[0],selftest[1],selftest[2],selftest[3]))

    
    print('Attempting to calibrate sensor\n')

    while True:
        if state == CALIBRATING:
            calib_stat = imu.get_calib()
            if (runs%100==0):
                print('MAG={:}\tACC={:}\tGYR={:}\tSYS={:}'.format(calib_stat[0],calib_stat[1],calib_stat[2],calib_stat[3]))
            
            if vcp.any():
                char = vcp.read()
            
                if char[0] == 71 or char[0] == 103:
                    print('MAG={:}\tACC={:}\tGYR={:}\tSYS={:}'.format(calib_stat[0],calib_stat[1],calib_stat[2],calib_stat[3]))
                    print('Sensor Calibrated\n')
                    th_offset=imu.get_roll()
                    state = GOING
    
        elif state == GOING:
            xd  = sec2m*(-M1.get_speed()+M2.get_speed())/2
            thd = deg2rad*imu.get_roll_rate()
            x   = sec2m*(-M1.get_position()+M2.get_position())/2
            th  = -deg2rad*(imu.get_roll()-th_offset)
            
            xd_filt  = alpha1*xd_filt +beta1*xd
            thd_filt = alpha2*thd_filt+beta2*thd
            x_filt   = alpha3*x_filt  +beta3*x
            th_filt  = alpha4*th_filt +beta4*th
            
            
            k_sched = 1#-abs(th_filt)
            
            duty = int(k_sched*ks3*xr +
                       k_sched*ks1*(-xd_filt) +
                       k_sched*ks2*(-thd_filt) +
                       k_sched*ks3*(-x_filt) +
                       k_sched*ks4*(-th_filt) )
            
            # duty = int(ks3*xr
                       # -ks1*xd +
                       # -ks2*thd +
                       # -ks3*x
                       # -ks4*th)                   
                       
            # duty_filt = alpha5*duty_filt +beta5*duty
            
            satmax = 1000
            
            if duty>satmax :
                duty = satmax
            elif duty<-satmax :
                duty=-satmax
            
            if th > -1 and th < 1 :
                M1.set_duty(duty)
                M2.set_duty(-duty)
            else :
                M1.set_duty(0)
                M2.set_duty(0)
                
            # if (runs%100==0):
                # t=utime.ticks_ms()
                # print_task.put('{:}\t{:.2f}\t{:.2f}\t{:.2f}\t{:.2f}\t{:}\n'.format(t,xd,thd,x,th,duty))
                # # print('{:},{:},{:},{:},{:},{:}\n'.format(t,xd,thd,x,th,duty))
        else:
            raise Exception('Invalid state')

        runs += 1
        yield (state)

q0 = task_share.Queue ('f', 1000, thread_protect = False, overwrite = False,
                       name = "Queue_0")
q1 = task_share.Queue ('I', 1000, thread_protect = False, overwrite = False,
                       name = "Queue_1")

def task2_fun():
    state = 0
    M1.set_duty(200)
    M2.set_duty(200)
    while True:
        if state == 0:
            q0.put(imu.get_gyro()[1])
            q1.put(utime.ticks_ms())
            if q0.full():
                state = 1
                print('Done collecting')
        elif state == 1:
            print('{:},{:}'.format(q1.get(),q0.get()))
            if q0.empty():
                state = 2
        yield()
        
## Task object associated with the task 1 function
task1 = cotask.Task (task1_fun, name = 'Motor Task', priority = 1, 
                     period = 4, profile = True, trace = False)
task2 = cotask.Task (task2_fun, name = 'IMU Task', priority = 1, 
                     period = 5, profile = True, trace = False)
cotask.task_list.append (task1)
# cotask.task_list.append (task2)

## Virtual com port object used for serial comms
vcp = pyb.USB_VCP ()

def run_sched():
    global k1, k2, k3, k4
    global ks1, ks2, ks3, ks4
    global alpha1, alpha2, beta3, beta4, alpha5
    global beta1, beta2, beta3, beta4, beta5
    
    Vdc = 8                 # DC voltage
    pwm_per = 2000          # clock ticks
    R = 1.8                 # ohms
    kt = 0.035              # Nm/A
    
    kscale = (1/kt)*(R/Vdc)*pwm_per
    
    
    ks1 = kscale*k1
    ks2 = kscale*k2
    ks3 = kscale*k3
    ks4 = kscale*k4
    
    beta1 = 1-alpha1
    beta2 = 1-alpha2
    beta3 = 1-alpha3
    beta4 = 1-alpha4
    beta5 = 1-alpha5
    
#    while not vcp.any ():
    while True:
        cotask.task_list.pri_sched ()

    # Empty the comm port buffer of the character(s) just pressed
#    vcp.read ()

    # Print a table of task data and a table of shared information data
#    print ('\n' + str (cotask.task_list) + '\n')
#    print (task_share.show_all ())
#    print (task1.get_trace ())
#    print ('\r\n')
    
run_sched()