##
#  @file bldc_hall.py
#  This module contains a class that will allow commutation of a BLDC motor
#  using hall sensors

import pyb

##
#  @brief This class facilitates reading from a set of hall sensors and
#         commutating a brushless dc motor
class BLDC_Hall:

    ##
    #  @brief Constructor for BLDC_Hall
    #
    #  @param hall_tim    The timer used for hall sensor input capture and
    #                     commutation interrupts
    #  @param hall_A_pin  The pin used for hall sensor channel A
    #  @param hall_B_pin  The pin used for hall sensor channel B
    #  @param hall_C_pin  The pin used for hall sensor channel C
    #  @param PWM_tim     The timer used for PWM on the motor phases
    #  @param PWM_U_pin   The pin used for PWM output on phase U
    #  @param PWM_V_pin   The pin used for PWM output on phase V
    #  @param PWM_W_pin   The pin used for PWM output on phase W
    #  @param EN_U_pin    The pin used for enabling output on phase U
    #  @param EN_V_pin    The pin used for enabling output on phase V
    #  @param EN_W_pin    The pin used for enabling output on phase W
    #  @param nSLEEP_pin  The pin used to enable or disable the motor driver
    #  @param nRESET_pin  The pin used to reset or clear fault status
    #  @param nFAULT_pin  The pin used to detect fault on the motor driver
    #
    #  @details
    #  Configures two timers:
    #  -# Timer used for PWM
    #  -# Timer used for Hall sensor input capture and for commutation
    #     using an OC interrupt on the same timer.
    def __init__(self,
                 hall_tim_num,
                 hall_A_pin,
                 hall_B_pin,
                 hall_C_pin,
                 PWM_tim_num,
                 PWM_U_pin,
                 PWM_V_pin,
                 PWM_W_pin,
                 EN_U_pin,
                 EN_V_pin,
                 EN_W_pin,
                 nSLEEP_pin,
                 nRESET_pin,
                 nFAULT_pin):
        
        ## Pin used for enabling the motor driver
        self._nSLEEP_pin = nSLEEP_pin
        self._nSLEEP_pin.init(mode=pyb.Pin.OUT_PP)
        
        ## Pin used to reset the motor driver if a fault occurs
        self._nRESET_pin = nRESET_pin
        self._nRESET_pin.init(mode=pyb.Pin.OUT_PP)
        self._nRESET_pin.high()
        
        ## Pin used to indicate fault on the motor driver
        self._nFAULT_pin = nFAULT_pin
        self._nFAULT_pin.init(mode=pyb.Pin.IN)
        
        ## The difference in duty cycle for the positive and negative
        #  phases of the PWM output
        self._duty = 999

        ## The duty cycle for the positive phase of the PWM output
        self._pos_duty = 999

        ## The duty cycle for the negative phase of the PWM output
        self._neg_duty = 999

        ## The current position of the motor in sectors
        self._position = 0

        ## The current speed of the motor in sectors per second
        self._speed = 0

        ## The fault status of the motor. Indicates that an invalid
        #  sector has been detected (000 or 111).
        self._fault = False

        ## The current sector of the motor
        self._sector = 0

        ## The most recent sector of the motor
        self._last_sector = 0

        ## The timer value at the current hall A IC interrupt
        self._ticksA = 0

        ## The timer value at the current hall B IC interrupt
        self._ticksB = 0

        ## The timer value at the current hall C IC interrupt
        self._ticksC = 0

        ## The timer value at the last hall A IC interrupt
        self._last_ticksA = 0

        ## The timer value at the last hall B IC interrupt
        self._last_ticksB = 0

        ## The timer value at the last hall C IC interrupt
        self._last_ticksC = 0

        ## The difference in timer value between the current and lash
        #  hall IC interrupt for whichever channel was most recent.
        self._dticks = 0

        ## Flag indicating that the hall sensor IC timer has overflowed
        self._ovf_flag = False

        ## Overflow flag indicating next IC reading for hall A should be
        #  thrown out
        self._ovf_A_flag = False

        ## Overflow flag indicating next IC reading for hall B should be
        #  thrown out
        self._ovf_B_flag = False

        ## Overflow flag indicating next IC reading for hall C should be
        #  thrown out
        self._ovf_C_flag = False

        ## The time used for hall sensor IC interrupts
        self._hall_tim = pyb.Timer(hall_tim_num, prescaler=239, period=0xFFFF,
                                   callback=self._h_ovf_cb)

        if hall_tim_num == 2:
            ## The pin used to read from hall channel A and to trigger IC interrupts
            self._hA_pin = pyb.Pin(hall_A_pin, mode = pyb.Pin.ALT,
                                  af=pyb.Pin.AF1_TIM2, pull = pyb.Pin.PULL_UP)

            ## The pin used to read from hall channel B and to trigger IC interrupts
            self._hB_pin = pyb.Pin(hall_B_pin, mode = pyb.Pin.ALT,
                                  af=pyb.Pin.AF1_TIM2, pull = pyb.Pin.PULL_UP)

            ## The pin used to read from hall channel C and to trigger IC interrupts
            self._hC_pin = pyb.Pin(hall_C_pin, mode = pyb.Pin.ALT,
                                  af=pyb.Pin.AF1_TIM2, pull = pyb.Pin.PULL_UP)
        elif hall_tim_num == 4:
            ## The pin used to read from hall channel A and to trigger IC interrupts
            self._hA_pin = pyb.Pin(hall_A_pin, mode = pyb.Pin.ALT,
                                  af=pyb.Pin.AF2_TIM4, pull = pyb.Pin.PULL_UP)

            ## The pin used to read from hall channel B and to trigger IC interrupts
            self._hB_pin = pyb.Pin(hall_B_pin, mode = pyb.Pin.ALT,
                                  af=pyb.Pin.AF2_TIM4, pull = pyb.Pin.PULL_UP)

            ## The pin used to read from hall channel C and to trigger IC interrupts
            self._hC_pin = pyb.Pin(hall_C_pin, mode = pyb.Pin.ALT,
                                  af=pyb.Pin.AF2_TIM4, pull = pyb.Pin.PULL_UP)

        ## The timer channel used for hall sensor A IC interrupts
        self._hA = self._hall_tim.channel(1, mode=pyb.Timer.IC,
                                         callback=self._hA_cb,
                                         polarity=pyb.Timer.BOTH)

        ## The timer channel used for hall sensor B IC interrupts
        self._hB = self._hall_tim.channel(2, mode=pyb.Timer.IC,
                                         callback=self._hB_cb,
                                         polarity=pyb.Timer.BOTH)

        ## The timer channel used for hall sensor C IC interrupts
        self._hC = self._hall_tim.channel(3, mode=pyb.Timer.IC,
                                         callback=self._hC_cb,
                                         polarity=pyb.Timer.BOTH)

        print('Made and configured Timer for hall sensor')


        print('Initialized Sector')

        ## The timer used to generate PWM for the motor
        self._PWM_tim = pyb.Timer(PWM_tim_num, mode=pyb.Timer.CENTER,
                                  freq=40000, div=1, deadtime=0)
        print('Initialized timer for motor')

        ## The pin used for EN on phase U
        self._EN_U_pin = pyb.Pin(EN_U_pin, mode=pyb.Pin.OUT_PP)
        self._EN_U_pin.low()

        ## The pin used for EN on phase V
        self._EN_V_pin = pyb.Pin(EN_V_pin, mode=pyb.Pin.OUT_PP)
        self._EN_V_pin.low()

        ## The pin used for EN on phase V
        self._EN_W_pin = pyb.Pin(EN_W_pin, mode=pyb.Pin.OUT_PP)
        self._EN_W_pin.low()

        ## The timer channel used for PWM on phase U
        self._PWM_U = self._PWM_tim.channel(1, mode=pyb.Timer.PWM, pin=PWM_U_pin,
                                   pulse_width=999)

        ## The timer channel used for PWM on phase V
        self._PWM_V = self._PWM_tim.channel(2, mode=pyb.Timer.PWM, pin=PWM_V_pin,
                                   pulse_width=999)

        ## The timer channel used for PWM on phase W
        self._PWM_W = self._PWM_tim.channel(3, mode=pyb.Timer.PWM, pin=PWM_W_pin,
                                   pulse_width=999)

        print('Made Timer for motor')

        # This timer channel is used to generate commutation interrupts
        # self._hall_tim.channel(4, mode=pyb.Timer.OC_TIMING, callback=self._commutate,
                        # compare = 667)
                        
        self._PWM_H = None
        self._PWM_L = None
        self._PWM_Z = None
        self._find_sector();
        self.set_duty(0)

    ##
    #  @brief Callback for hall sensor channel A
    #
    #  @param cb_src The source of the callback (the timer generating the
    #                callback).
    def _hA_cb(self,cb_src):
        delta = self._find_sector()
        self.set_duty(self._duty)
        self._ticksA=cb_src.channel(1).capture()
        if not self._ovf_A_flag:
            self._dticks = delta*((self._ticksA-self._last_ticksA)&0xFFFF)
        self._last_ticksA=self._ticksA
        self._ovf_A_flag = False
        self._ovf_flag = False
        #self._commutate(None)

    ##
    #  @brief Callback for hall sensor channel B
    #
    #  @param cb_src The source of the callback (the timer generating the
    #                callback).
    def _hB_cb(self,cb_src):
        delta = self._find_sector()
        self.set_duty(self._duty)
        self._ticksB=cb_src.channel(2).capture()
        if not self._ovf_B_flag:
            self._dticks = delta*((self._ticksB-self._last_ticksB)&0xFFFF)
        self._last_ticksB=self._ticksB
        self._ovf_B_flag = False
        self._ovf_flag = False
        #self._commutate(None)

    ##
    #  @brief Callback for hall sensor channel C
    #
    #  @param cb_src The source of the callback (the timer generating the
    #                callback).
    def _hC_cb(self,cb_src):
        delta = self._find_sector()
        self.set_duty(self._duty)
        self._ticksC=cb_src.channel(3).capture()
        if not self._ovf_C_flag:
            self._dticks = delta*((self._ticksC-self._last_ticksC)&0xFFFF)
        self._last_ticksC=self._ticksC
        self._ovf_C_flag = False
        self._ovf_flag = False
        #self._commutate(None)

    ##
    #  @brief Callback for overflow on hall sensor timer
    #
    #  This callback is responsible for detecting overflow on the hall sensor
    #  timer so that zero velocity can be determined and the code is not stuck
    #  waiting for an incorrect timer value.
    #
    #  @param cb_src The source of the callback (the timer generating the
    #                callback).
    def _h_ovf_cb(self,cb_src):
        if self._ovf_flag or self._ovf_A_flag or self._ovf_B_flag or self._ovf_C_flag:
            self._dticks = 1e9

            self._ovf_A_flag = True
            self._ovf_B_flag = True
            self._ovf_C_flag = True

        self._ovf_flag = True

    ##
    #  @brief Determines the current sector based on the state of the hall
    #         sensor pins.
    #
    #  @return The change in sector since the last sector, either -1 or 1
    def _find_sector(self):
        if self._hA_pin.value():
            if self._hB_pin.value():
                if self._hC_pin.value():
                    # 111
                    # ZZZ
                    self._sector = -1
                    self._fault = True
                    self._EN_U_pin.low()
                    self._EN_V_pin.low()
                    self._EN_W_pin.low()
                    self._PWM_U.pulse_width(999)
                    self._PWM_V.pulse_width(999)
                    self._PWM_W.pulse_width(999)
                else:
                    # 110
                    # ZHL
                    self._sector = 1
                    self._EN_U_pin.low()
                    self._EN_V_pin.high()
                    self._EN_W_pin.high()
                    self._PWM_Z = self._PWM_U
                    self._PWM_H = self._PWM_V
                    self._PWM_L = self._PWM_W
            else:
                if self._hC_pin.value():
                    # 101
                    # HLZ
                    self._sector = 3
                    self._EN_U_pin.high()
                    self._EN_V_pin.high()
                    self._EN_W_pin.low()
                    self._PWM_H = self._PWM_U
                    self._PWM_L = self._PWM_V
                    self._PWM_Z = self._PWM_W
                else:
                    # 100
                    # HZL
                    self._sector = 2
                    self._EN_U_pin.high()
                    self._EN_V_pin.low()
                    self._EN_W_pin.high()
                    self._PWM_H = self._PWM_U
                    self._PWM_Z = self._PWM_V
                    self._PWM_L = self._PWM_W
        else:
            if self._hB_pin.value():
                if self._hC_pin.value():
                    # 011
                    # LZH
                    self._sector = 5
                    self._EN_U_pin.high()
                    self._EN_V_pin.low()
                    self._EN_W_pin.high()
                    self._PWM_L = self._PWM_U
                    self._PWM_Z = self._PWM_V
                    self._PWM_H = self._PWM_W
                else:
                    # 010
                    # LHZ
                    self._sector = 6
                    self._EN_U_pin.high()
                    self._EN_V_pin.high()
                    self._EN_W_pin.low()
                    self._PWM_L = self._PWM_U
                    self._PWM_H = self._PWM_V
                    self._PWM_Z = self._PWM_W
            else:
                if self._hC_pin.value():
                    # 001
                    # ZLH
                    self._sector = 4
                    self._EN_U_pin.low()
                    self._EN_V_pin.high()
                    self._EN_W_pin.high()
                    self._PWM_Z = self._PWM_U
                    self._PWM_L = self._PWM_V
                    self._PWM_H = self._PWM_W
                else:
                    # 000
                    # ZZZ
                    self._sector = -1
                    self._fault = True
                    self._EN_U_pin.low()
                    self._EN_V_pin.low()
                    self._EN_W_pin.low()
                    self._PWM_U.pulse_width(999)
                    self._PWM_V.pulse_width(999)
                    self._PWM_W.pulse_width(999)

        delta = self._sector - self._last_sector

        if (delta == 1 or delta == -5):
            delta = 1
        elif(delta == -1 or delta == 5):
            delta = -1

        self._position = self._position+delta
        self._last_sector = self._sector
        return delta

    ##
    #  @brief Getter function to query the current sector
    #
    #  @return The current hall sector
    def get_sector(self):
        return self._sector

    ##
    #  @brief Commutation callback used to update the PWM going to the motor
    #         based on the current sector and desired duty cycle
    #
    #  @param cb_src The source of the callback (the timer generating the
    #                callback).
    def _commutate(self, cb_src):
        irqstate = pyb.disable_irq()

        if cb_src is not None:
            cb_src.channel(4).compare((cb_src.channel(4).compare()+667)&0xFFFF)


        # if self._fault:
            # self._EN_U_pin.low()
            # self._EN_V_pin.low()
            # self._EN_W_pin.low()
            # self._PWM_U.pulse_width(999)
            # self._PWM_V.pulse_width(999)
            # self._PWM_W.pulse_width(999)
            # print('FAULT - INVALID SECTOR')
        # else:

        # sec = self._sector

        # if sec == 1:
            # # ZHL
            # self._EN_U_pin.low()
            # self._EN_V_pin.high()
            # self._EN_W_pin.high()
            # self._PWM_U.pulse_width(999)
            # self._PWM_V.pulse_width(self._pos_duty)
            # self._PWM_W.pulse_width(self._neg_duty)

        # elif sec == 2:
            # # HZL
            # self._EN_U_pin.high()
            # self._EN_V_pin.low()
            # self._EN_W_pin.high()
            # self._PWM_U.pulse_width(self._pos_duty)
            # self._PWM_V.pulse_width(999)
            # self._PWM_W.pulse_width(self._neg_duty)

        # elif sec == 3:
            # # HLZ
            # self._EN_U_pin.high()
            # self._EN_V_pin.high()
            # self._EN_W_pin.low()
            # self._PWM_U.pulse_width(self._pos_duty)
            # self._PWM_V.pulse_width(self._neg_duty)
            # self._PWM_W.pulse_width(999)

        # elif sec == 4:
            # # ZLH
            # self._EN_U_pin.low()
            # self._EN_V_pin.high()
            # self._EN_W_pin.high()
            # self._PWM_U.pulse_width(999)
            # self._PWM_V.pulse_width(self._neg_duty)
            # self._PWM_W.pulse_width(self._pos_duty)

        # elif sec == 5:
            # # LZH
            # self._EN_U_pin.high()
            # self._EN_V_pin.low()
            # self._EN_W_pin.high()
            # self._PWM_U.pulse_width(self._neg_duty)
            # self._PWM_V.pulse_width(999)
            # self._PWM_W.pulse_width(self._pos_duty)

        # elif sec == 6:
            # # LHZ
            # self._EN_U_pin.high()
            # self._EN_V_pin.high()
            # self._EN_W_pin.low()
            # self._PWM_U.pulse_width(self._neg_duty)
            # self._PWM_V.pulse_width(self._pos_duty)
            # self._PWM_W.pulse_width(999)

        # else:
            # self._EN_U_pin.low()
            # self._EN_V_pin.low()
            # self._EN_W_pin.low()
            # self._PWM_U.pulse_width(999)
            # self._PWM_V.pulse_width(999)
            # self._PWM_W.pulse_width(999)
            # self._fault = True

        pyb.enable_irq(irqstate)

    ##
    #  @brief Getter fuction to query the speed of the motor
    #
    #  This method uses the difference in clock ticks between the two most
    #  recent hall sensor edges to determine the speed of the motor
    #
    #  @return The speed of the motor in sectors per second.
    def get_speed(self):
        if self._dticks == 1e9:
            self._speed = 0
        elif self._dticks > 625 or self._dticks < -625:
            self._speed = 1e6 / self._dticks
        return self._speed

    ##
    #  @brief Getter function to query the position of the motor
    #
    #  This method returns the signed number of sectors indicating the
    #  displacement of the motor.
    #
    #  @return Position of the motor in sectors
    def get_position(self):
        return self._position

    ##
    #  @brief Setter function to adjust or reset the position of the motor
    #
    #  @param position The position of the motor in sectors
    def set_position(self, position):
        self._position = position

    ##
    #  @brief Setter function to set a new duty cycle for the motor
    #
    #  This method saturates the duty cycle at plus or minus the PWM period
    #  and computes the adjusted duty cycles for the positive and negative
    #  phases.
    #
    #  @verbatim
    #  pos_duty = (period + duty)/2
    #  neg_duty = (period - duty)/2
    #  @endverbatim
    #
    #  Where period is the number of clock cycles per PWM cycle.
    #
    #  @param duty The duty cycle as an integer representing the desired
    #              difference in clock ticks between the positive and
    #              negative phases of the motor.
    def set_duty(self, duty):
        if duty > 1999:
            self._duty = 1999
        elif duty < -1999:
            self._duty = -1999
        else:
            self._duty = duty

        self._PWM_Z.pulse_width(999)
        self._PWM_H.pulse_width(1999+self._duty>>1)
        self._PWM_L.pulse_width(1999-self._duty>>1)

    ##
    #  @brief Returns fault state of the motor driver
    #  @return Fault state of the driver, 0 if a fault is present, 1if no fault
    #          is present.
    def get_fault_status(self):
        return self._nFAULT_pin.value()

    ##
    #  @brief Clears a fault if one is present
    def clear_fault(self):
        self._nRESET_pin.low()
        self._nRESET_pin.high()

    ##
    #  @brief Enables the motor driver
    def enable(self):
        self._nSLEEP_pin.high()

    ##
    #  @brief Disables the motor driver        
    def disable(self):
        self._nSLEEP_pin.low()