import micropython
import pyb
import utime

# BNO055 Slave Address
SLAVE_ADDR = micropython.const(40)

# BNO055 Configuration and Status Registers
AXIS_MAP_SIGN = micropython.const(0x42)
AXIS_MAP_CONFIG = micropython.const(0x41)
TEMP_SOURCE = micropython.const(0x40)
SYS_TRIGGER = micropython.const(0x3F)
PWR_MODE = micropython.const(0x3E)
OPR_MODE = micropython.const(0x3D)
UNIT_SEL = micropython.const(0x3B)
SYS_ERR = micropython.const(0x3A)
SYS_STATUS = micropython.const(0x39)
SYS_CLK_STATUS = micropython.const(0x38)
INT_STA = micropython.const(0x37)
ST_RESULT = micropython.const(0x36)
CALIB_STAT = micropython.const(0x35)
TEMP = micropython.const(0x34)

# BNO055 Gravity Vector Registers
GRV_Data_Z_MSB = micropython.const(0x33)
GRV_Data_Z_LSB = micropython.const(0x32)
GRV_Data_Y_MSB = micropython.const(0x31)
GRV_Data_Y_LSB = micropython.const(0x30)
GRV_Data_X_MSB = micropython.const(0x2F)
GRV_Data_X_LSB = micropython.const(0x2E)

# BNO055 Linear Acceleration Registers
LIA_Data_Z_MSB = micropython.const(0x2D)
LIA_Data_Z_LSB = micropython.const(0x2C)
LIA_Data_Y_MSB = micropython.const(0x2B)
LIA_Data_Y_LSB = micropython.const(0x2A)
LIA_Data_X_MSB = micropython.const(0x29)
LIA_Data_X_LSB = micropython.const(0x28)

# BNO055 Quaternion Registers
QUA_Data_Z_MSB = micropython.const(0x27)
QUA_Data_Z_LSB = micropython.const(0x26)
QUA_Data_Y_MSB = micropython.const(0x25)
QUA_Data_Y_LSB = micropython.const(0x24)
QUA_Data_X_MSB = micropython.const(0x23)
QUA_Data_X_LSB = micropython.const(0x22)
QUA_Data_W_MSB = micropython.const(0x21)
QUA_Data_W_LSB = micropython.const(0x20)

# BNO055 Euler Angle Registers
EUL_Pitch_MSB = micropython.const(0x1F)
EUL_Pitch_LSB = micropython.const(0x1E)
EUL_Roll_MSB = micropython.const(0x1D)
EUL_Roll_LSB = micropython.const(0x1C)
EUL_Heading_MSB = micropython.const(0x1B)
EUL_Heading_LSB = micropython.const(0x1A)

# BNO055 Gyroscope Registers
GYR_DATA_Z_MSB = micropython.const(0x19)
GYR_DATA_Z_LSB = micropython.const(0x18)
GYR_DATA_Y_MSB = micropython.const(0x17)
GYR_DATA_Y_LSB = micropython.const(0x16)
GYR_DATA_X_MSB = micropython.const(0x15)
GYR_DATA_X_LSB = micropython.const(0x14)

# BNO055 Magnetometer Registers
MAG_DATA_Z_MSB = micropython.const(0x13)
MAG_DATA_Z_LSB = micropython.const(0x12)
MAG_DATA_Y_MSB = micropython.const(0x11)
MAG_DATA_Y_LSB = micropython.const(0x10)
MAG_DATA_X_MSB = micropython.const(0x0F)
MAG_DATA_X_LSB = micropython.const(0x0E)

# BNO055 Accelerometer Registers
ACC_DATA_Z_MSB = micropython.const(0x0D)
ACC_DATA_Z_LSB = micropython.const(0x0C)
ACC_DATA_Y_MSB = micropython.const(0x0B)
ACC_DATA_Y_LSB = micropython.const(0x0A)
ACC_DATA_X_MSB = micropython.const(0x09)
ACC_DATA_X_LSB = micropython.const(0x08)

# BNO055 MISC Registers
PAGE_ID = micropython.const(0x07)
BL_Rev_ID = micropython.const(0x06)
SW_REV_ID_MSB = micropython.const(0x05)
SW_REV_ID_LSB = micropython.const(0x04)

# BNO055 Chip ID Registers
GRY_ID = micropython.const(0x03)
MAG_ID = micropython.const(0x02)
ACC_ID = micropython.const(0x01)
CHIP_ID  = micropython.const(0x00)

## @brief Sensor interface for Bosch BNO055
class BNO055:
    ## 
    #  @brief Constructs sensor object and queries the chip ID register to
    #         confirm communication with the sensor.
    #
    #  @param i2c A pyb.I2C object configured for master mode
    #
    #  @return True if the chip ID is queried properly, False otherwise.
    def __init__(self,i2c):
        ## The pyb.I2C object used for interacting with the sensor
        self._i2c = i2c
        
        ## A flag indicating the sensor is ready to interact with
        self._ready = False
        
        if (self._i2c.mem_read(1,SLAVE_ADDR, CHIP_ID)[0] == 0xA0):
            self._ready = True
            self._i2c.mem_write(0x00,SLAVE_ADDR,OPR_MODE)
    
    ## 
    #  @brief Enables nine-degree-of-freedom mode for the IMU. This activates
    #         internal filter fusing the data between the gyroscope,
    #         accelerometer, and magnetometer.
    #
    #  @return The operating mode byte value from the sensor is returned if the
    #          sensor is ready, if not False is returned.
    def enable_ndof(self):
        if (self._ready):
            self._i2c.mem_write(0x00,SLAVE_ADDR,PWR_MODE)
            self._i2c.mem_write(0x00,SLAVE_ADDR,AXIS_MAP_SIGN)
#            self._i2c.mem_write(0x0C,SLAVE_ADDR,OPR_MODE)
            self._i2c.mem_write(0x0B,SLAVE_ADDR,OPR_MODE)
            return self._i2c.mem_read(1,SLAVE_ADDR,OPR_MODE)
        else:
            return False
    
    ## 
    #  @brief Enables six-degree-of-freedom mode for the IMU. This activates
    #         internal filter fusing the data between the gyroscope and
    #         accelerometer, but not the magnetometer.
    #
    #  @return The operating mode byte value from the sensor is returned if the
    #          sensor is ready, if not False is returned.
    def enable_imu(self):
        if (self._ready):
            self._i2c.mem_write(0x00,SLAVE_ADDR,PWR_MODE)
            self._i2c.mem_write(0x00,SLAVE_ADDR,AXIS_MAP_SIGN)
            self._i2c.mem_write(0x08,SLAVE_ADDR,OPR_MODE)
            return self._i2c.mem_read(1,SLAVE_ADDR,OPR_MODE)
        else:
            return False
    
    ## 
    #  @brief Gets the euler angles representing the orientation of the sensor.
    #         This will only return valid numbers if NDOF mode is active.
    #
    #  @return The euler angles are returned from the sensor if the sensor is
    #          enabled properly, otherwise False is returned. Unist are degrees.
    def get_euler(self):
        if (self._ready):
            data = self._i2c.mem_read(6,SLAVE_ADDR,EUL_Heading_LSB)
            
            heading = (data[0] | data[1]<<8)
            if (heading > 32767):
                heading -= 65536
                
            roll    = (data[2] | data[3]<<8)
            if (roll > 32767):
                roll -= 65536
                
            pitch   = (data[4] | data[5]<<8)
            if (pitch > 32767):
                pitch -= 65536
                
            return (heading/16, roll/16, pitch/16)
        else:
            return False
    
    ## 
    #  @brief Gets just the roll angle representing the orientation of the sensor.
    #         This will only return valid numbers if NDOF mode is active.
    #
    #  @return The roll angle is returned from the sensor if the sensor is
    #          enabled properly, otherwise False is returned. Units are degrees.
    def get_roll(self):
        if (self._ready):
            data = self._i2c.mem_read(2,SLAVE_ADDR,EUL_Roll_LSB)
            
            roll = (data[0] | data[1]<<8)
            if (roll > 32767):
                roll -= 65536
                
            return roll/16
        else:
            return False
    
    ## 
    #  @brief Gets the angular velocity of the sensor. If the sensor is in NDOF
    #         mode then the filtered angular velocity will be queried, otherwise
    #         the raw gyroscope data will be queried.
    #
    #  @return The angular velocity is returned from the sensor if the sensor is
    #          enabled properly, otherwise False is returned. Units are
    #          degrees/second
    def get_gyro(self):
        if (self._ready):
            data = self._i2c.mem_read(6,SLAVE_ADDR,GYR_DATA_X_LSB)
            
            w_x = (data[0] | data[1]<<8)
            if (w_x > 32767):
                w_x -= 65536
                
            w_y = (data[2] | data[3]<<8)
            if (w_y > 32767):
                w_y -= 65536
                
            w_z = (data[4] | data[5]<<8)
            if (w_z > 32767):
                w_z -= 65536
                
            return (w_x/16, w_y/16, w_z/16)
        else:
            return False
    
    ## 
    #  @brief Gets the roll velocity of the sensor. If the sensor is in NDOF
    #         mode then the filtered angular velocity will be queried, otherwise
    #         the raw gyroscope data will be queried.
    #
    #  @return The roll velocity is returned from the sensor if the sensor is
    #          enabled properly, otherwise False is returned. Units are 
    #          degrees/second.
    def get_roll_rate(self):
        if (self._ready):
            data = self._i2c.mem_read(6,SLAVE_ADDR,GYR_DATA_Y_LSB)
            
            w_y = (data[0] | data[1]<<8)
            if (w_y > 32767):
                w_y -= 65536
                
            return w_y/16
        else:
            return False
            
    ## 
    #  @brief Gets the quaternion representing the orientation of the sensor.
    #         This will only return valid numbers if NDOF mode is active.
    #
    #  @return The quaternion is returned from the sensor if the sensor is
    #          enabled properly, otherwise False is returned.
    def get_quat(self):
        if (self._ready):
            data = self._i2c.mem_read(8,SLAVE_ADDR,QUA_Data_W_LSB)
            
            q_w = (data[2] | data[3]<<8)
            if (q_w > 32767):
                q_w -= 65536
            
            q_x = (data[2] | data[3]<<8)
            if (q_x > 32767):
                q_x -= 65536
                
            q_y = (data[4] | data[5]<<8)
            if (q_y > 32767):
                q_y -= 65536
                
            q_z = (data[6] | data[7]<<8)
            if (q_z > 32767):
                q_z -= 65536
                
            return (q_w/16384, q_x/16384, q_y/16384, q_z/16384)
        else:
            return False
    
    ## 
    #  @brief Applies a rotation to a vector through a quaternion product. If
    #         the conjugate parameter is set it will perform an inverse
    #         rotation.
    #
    #  @param q The quaternion representing the desired rotation
    #  @param u The vector being rotated
    #  @param conj A flag indicating that an inverse rotation is desired.
    #
    #  @return The rotated vector
    def rotate_vector(self, q, u, conj=True):
        
        if conj:
            a1 = q[0]
            b1 = -q[1]
            c1 = -q[2]
            d1 = -q[3]
        else:
            a1 = q[0]
            b1 = q[1]
            c1 = q[2]
            d1 = q[3]
        
        a2 = 0
        b2 = u[0]
        c2 = u[1]
        d2 = u[2]
        
        v = (a1*a2 - b1*b2 - c1*c2 - d1*d2,
             a1*b2 + b1*a2 + c1*d2 - d1*c2,
             a1*c2 - b1*d2 + c1*a2 + d1*b2,
             a1*d2 + b1*c2 - c1*b2 + d1*a2)
        
        a2 = a1
        b2 = -b1
        c2 = -c1
        d2 = -d1
        
        a1 = v[0]
        b1 = v[1]
        c1 = v[2]
        d1 = v[3]
        
        
        w = (a1*a2 - b1*b2 - c1*c2 - d1*d2,
             a1*b2 + b1*a2 + c1*d2 - d1*c2,
             a1*c2 - b1*d2 + c1*a2 + d1*b2,
             a1*d2 + b1*c2 - c1*b2 + d1*a2)
        
        return (w[1], w[2], w[3])
    
    ##
    #  @brief Queries the calibration status of the sensor
    #
    #  @return A tuple containing the calibration status for the sensor or
    #          False if the sensor is not ready.
    def get_calib(self):
        if (self._ready):
            data = self._i2c.mem_read(1,SLAVE_ADDR,CALIB_STAT)
            
            sys_cal = (data[0] >> 6) & 0x03
            gyr_cal = (data[0] >> 4) & 0x03
            acc_cal = (data[0] >> 2) & 0x03
            mag_cal = (data[0] >> 0) & 0x03
                
            return (mag_cal, acc_cal, gyr_cal, sys_cal)
        else:
            return False
    
    ##
    #  @brief Queries the self test status of the sensor
    #
    #  @return A tuple containing the self test status for the sensor or
    #          False if the sensor is not ready.
    def get_selftest(self):
        if (self._ready):
            data = self._i2c.mem_read(1,SLAVE_ADDR,ST_RESULT)
            
            mcu_st = (data[0] >> 3) & 0x01
            gyr_st = (data[0] >> 2) & 0x01
            acc_st = (data[0] >> 1) & 0x01
            mag_st = (data[0] >> 0) & 0x01
                
            return (mag_st, acc_st, gyr_st, mcu_st)
        else:
            return False