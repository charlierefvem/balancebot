filename = uigetfile('*.csv');

%%
data = struct();
data.raw=zeros(0,17);

%%
fID = fopen(filename);
astr = fread(fID,'*char')';
fclose(fID);
str_arr = split(astr,newline);
str_arr(1) = [];
N=length(str_arr)-1;
check = zeros(N,1);
for n=1:N
    char_arr = split(str_arr(n,:),',');
    check(n) = 0;
    if length(char_arr) ~= 18
        continue;
    end
    for m=1:17
        check(n) = check(n) + sum(double(char_arr{m}));
    end
    check(n) = check(n) + 16*double(',');
    check(n) = mod(check(n),256);
    
    if(check(n) == str2double(char_arr{18}))
        row = size(data.raw,1) + 1;
        for m=1:17
            data.raw(row,m) = str2double(char_arr{m});
        end
    end
end
%%
% data.raw = readmatrix(filename);
data.time = data.raw(:,1)/10^6;
data.time = data.time - data.time(1);
data.dt = diff(data.time);
data.avg_dt = (data.time(end) - data.time(1))/(length(data.time) - 1);
data.input = data.raw(:,2:3);
data.int_input = cumtrapz(data.time,data.input);
data.x_des = data.raw(:,[4,6,8,10,12,14]);
data.x = data.raw(:,[5,7,9,11,13,15]);
data.actuation = data.raw(:,16:17);
data.error = data.x_des-data.x;
data.rms_error = vecnorm(data.error,2,2);

%% Plot styling
fontsize = 10;
t_lim = [0 100];

%% Clear plots
close all;

%% State 1 - Motor Angle
hf1=figure(1);
    hf1.Renderer = 'painters';
    hf1.Name = 'x_1';
    pos=hf1.Position; pos(4) = 280;
    hf1.Position = pos;
    plot(data.time,data.int_input(:,1),'k','linewidth',1,'DisplayName','Integrated Throttle Input');
    hold on;
    plot(data.time,data.x_des(:,1),'--k','linewidth',1,    'DisplayName','Desired Motor Angle');
    plot(data.time,data.x(:,1),':k','linewidth',1,        'DisplayName','Measured Motor Angle');
    hold off;
    legend('Location','best','interpreter','latex','fontsize',fontsize);
    xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
    ylabel('$y_1~[rad]$','interpreter','latex','fontsize',fontsize);
%     title('$x_1$: Motor Angle','interpreter','latex');
    xlim(t_lim);
    save2pdf('T1_HW_x1.pdf',gcf,600);

%% State 2 - Pitch Angle
hf2 = figure(2);
    hf2.Renderer = 'painters';
    hf2.Name = 'x_2';
    pos=hf2.Position; pos(4) = 280;
    hf2.Position = pos;
    plot(data.time,data.x_des(:,2),'k','linewidth',1,    'DisplayName','Desired Pitch Angle');
    hold on;
    plot(data.time,data.x(:,2),'--k','linewidth',1,        'DisplayName','Measured Pitch Angle');
    hold off;
    legend('Location','best','interpreter','latex','fontsize',fontsize);
    xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
    ylabel('$y_2~[rad]$','interpreter','latex','fontsize',fontsize);
%     title('$x_2$: Pitch Angle','interpreter','latex');
    xlim(t_lim);
    save2pdf('T1_HW_x2.pdf',gcf,600);
    

%% State 3 - Yaw Angle
hf3 = figure(3);
    hf3.Renderer = 'painters';
    hf3.Name = 'x_3';
    pos=hf3.Position; pos(4) = 280;
    hf3.Position = pos;
    plot(data.time,data.int_input(:,2),'k','linewidth',1,'DisplayName','Integrated Steering Input');
    hold on;
    plot(data.time,data.x_des(:,3),'--k','linewidth',1,    'DisplayName','Desired Yaw Angle');
    plot(data.time,data.x(:,3),':k','linewidth',1,        'DisplayName','Measured Yaw Angle');
    hold off;
    legend('Location','best','interpreter','latex','fontsize',fontsize);
    xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
    ylabel('$y_3~[rad]$','interpreter','latex','fontsize',fontsize);
%     title('$x_3$: Yaw Angle','interpreter','latex');
    xlim(t_lim);
    save2pdf('T1_HW_x3.pdf',gcf,600);

%% State 4 -  Motor Velocity
hf4 = figure(4);
    hf4.Renderer = 'painters';
    hf4.Name = 'x_4';
    pos=hf4.Position; pos(4) = 280;
    hf4.Position = pos;
    plot(data.time,data.input(:,1),'k','linewidth',1,'DisplayName','Throttle Input');
    hold on;
    plot(data.time,data.x_des(:,4),'--k','linewidth',1,'DisplayName','Desired Motor Velocity');
    plot(data.time,data.x(:,4),':k','linewidth',1,'DisplayName','Measured Motor Velocity');
    hold off;
    legend('Location','best','interpreter','latex','fontsize',fontsize);
    xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
    ylabel('$y_4~[rad/s]$','interpreter','latex','fontsize',fontsize);
%     title('$x_4$: Motor Speed','interpreter','latex');
    xlim(t_lim);
    save2pdf('T1_HW_x4.pdf',gcf,600);

%% State 5 - Pitch Rate
hf5 = figure(5);
    hf5.Renderer = 'painters';
    hf5.Name = 'x_5';
    pos=hf5.Position; pos(4) = 280;
    hf5.Position = pos;
    plot(data.time,data.input(:,1),'k','linewidth',1,'DisplayName','Throttle Input');
    hold on;
    plot(data.time,data.x_des(:,5),'--k','linewidth',1,'DisplayName','Desired Pitch Rate');
    plot(data.time,data.x(:,5),':k','linewidth',1,    'DisplayName','Measured Pitch Rate');
    hold off;
    legend('Location','best','interpreter','latex','fontsize',fontsize);
    xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
    ylabel('$y_5~[rad/s]$','interpreter','latex','fontsize',fontsize);
%     title('$x_5$: Pitch Rate','interpreter','latex');
    xlim(t_lim);
    save2pdf('T1_HW_x5.pdf',gcf,600);

%% State 6 - Yaw Rate
hf6 = figure(6);
    hf6.Renderer = 'painters';
    hf6.Name = 'x_6';
    pos=hf6.Position; pos(4) = 280;
    hf6.Position = pos;
    plot(data.time,data.input(:,2),'k','linewidth',1,'DisplayName','Steering Input');
    hold on;
    plot(data.time,data.x_des(:,6),'--k','linewidth',1,'DisplayName','Desired Yaw Rate');
    plot(data.time,data.x(:,6),':k','linewidth',1,    'DisplayName','Measured Yaw Rate');
    hold off;
    legend('Location','best','interpreter','latex','fontsize',fontsize);
    xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
    ylabel('$y_6~[rad/s]$','interpreter','latex','fontsize',fontsize);
%     title('$x_6$: Yaw Rate','interpreter','latex');
    xlim(t_lim);
    save2pdf('T1_HW_x6.pdf',gcf,600);

%% Mean Square Error
hf7 = figure(7);
    hf7.Renderer = 'painters';
    hf7.Name = 'MSE';
    pos=hf7.Position; pos(4) = 280;
    hf7.Position = pos;
    plot(data.time,data.rms_error,'k','linewidth',1,'DisplayName','Mean Squared Error');
    xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
    ylabel('Mean Squared Error','interpreter','latex','fontsize',fontsize);
%     legend('Location','best','interpreter','latex','fontsize',fontsize);
%     title('Mean Squared Error','interpreter','latex');
    xlim(t_lim);
    save2pdf('T1_HW_MSE.pdf',gcf,600);
    
%% Time-keeping variance
hf8 = figure(8);
    hf8.Renderer = 'painters';
    hf8.Name = 'MSE';
    pos=hf8.Position; pos(4) = 280;
    hf8.Position = pos;
%     plot(data.time(1:end-1),1000*(data.dt-.025),'k','linewidth',1,'DisplayName','Time-keeping Variance');
    histogram(1000*data.dt,[20:0.25:30],'Normalization','probability','facecolor','k')
    xlabel('Loop Period, $\Delta t ~[ms]$','interpreter','latex','fontsize',fontsize);
    ylabel('Normalized Probability','interpreter','latex','fontsize',fontsize);
%     legend('Location','best','interpreter','latex','fontsize',fontsize);
%     title('Mean Squared Error','interpreter','latex');
    xticks([20:2.5:30]);
    text(26,0.2,sprintf('Std. Dev. = %2.2f~[ms]',std(1000*data.dt)),'interpreter','latex','fontsize',fontsize);
    save2pdf('T1_HW_deltat.pdf',gcf,600);