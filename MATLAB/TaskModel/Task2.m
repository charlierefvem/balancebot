classdef Task2 < SuperTask
    properties
    end
    methods
        function obj = Task2(name, period)
            obj=obj@SuperTask(name,period);
        end

        function [] = fsm(obj,t)
            switch obj.state
                case 0
                    obj.state = 1;
                case 1
                otherwise
                    warning('Invalid state in task %s',obj.name);
            end
        end
    end
end