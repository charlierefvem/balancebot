classdef TaskList < handle
    properties
        tasks = {}
        t = 0
        dt
    end
    methods
        function obj = TaskList(dt)
            obj.dt = dt;
        end
        
        function [] = AddTask(obj, task)
            obj.tasks{end+1} = task;
        end
        
        function [] = loop(obj,iter)
            for k=1:iter
                for n=1:length(obj.tasks)
                    obj.tasks{n}.run(obj.t);
                end
                obj.t = obj.t+obj.dt;
            end
        end
    end
end