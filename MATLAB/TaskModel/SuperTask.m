classdef SuperTask < handle
    properties
        state = 0
        name
        runs = 0
        trace = []
        period
        last_t
        tol = 1e-9
    end
    methods
        function obj = SuperTask(name, period)
            obj.name = name;
            obj.period = period;
            obj.last_t = -period;
        end
        
        function [] = run(obj, t)
            if (t - obj.last_t) - obj.period >= -obj.tol
                obj.runs = obj.runs + 1;
                obj.trace(obj.runs,:) = [t obj.state];

                obj.fsm(t)
                
                obj.last_t = t;
            end
        end
        
        function [] = fsm(~,~)
             error('Unimplemented state machine');
        end
    end
end