classdef Task1 < SuperTask
    properties
        t    = [];          % Array to store time data
        x    = [];          % Array to store state data
        u    = [];
        
        mb   = 0.7;         % [kg] mass of body
        Ibxx = 10e-4;       % [kg*m^2] centroidal inertia about x of body
        Ibyy = 10e-4;       % [kg*m^2] centroidal inertia about y of body
        Ibzz = 5e-4;        % [kg*m^2] centroidal inertia about z of body

        mw   = 0.175;       % [kg] mass of wheel
        Iwxx = 0.5e-4;      % [kg*m^2] centroidal inertia about x of wheel
        Iwyy = 1.0e-4;      % [kg*m^2] centroidal inertia about y of wheel
        Iwzz = 0.5e-4;      % [kg*m^2] centroidal inertia about z of wheel

        rw   = 45e-3;       % [m] radius of wheel
        rb   = 60e-3;       % [m] distance from axle to CG of body
        w    = 115e-3;      % [m] distance between CG of wheels on axle

        g    = 9.81;        % [m/s/s] acceleration due to gravity

        Kv   = 0.043;       % [Nm/A] motor torque constant
        R    = 1.39;        % [ohm] winding resistance
        L    = 1.18e-3;     % [H] winding inductance
        
        mx   = 0;
        Ithe = 0;
        Ipsi = 0;
        
        A    = zeros(10);
        B    = zeros(10,4);
        C    = zeros(16,10);
        D    = zeros(16,4);
        
        K    = zeros(4,10); % Gain matrix for state feedback
        
    end
    methods
        function obj = Task1(name, period)
            obj=obj@SuperTask(name,period);
            
            
            obj.mx   = obj.mb + 2*obj.mw + 2* obj.Iwyy/obj.rw^2;
            obj.Ithe = obj.mb*obj.rb^2+obj.Ibyy;
            obj.Ipsi = obj.mw*obj.w^2/2 + obj.Iwyy/obj.rw^2*obj.w^2/2 + 2*obj.Iwzz;
            
            M = zeros(3);
            M(1,1) = obj.mb + 2*obj.mw + 2*obj.Iwyy/obj.rw^2;
            M(1,2) = obj.mb*obj.rb;
            M(2,1) = obj.mb*obj.rb;
            M(2,2) = obj.mb*obj.rb^2 + obj.Ibyy;
            M(3,3) = obj.Ibzz + 2*obj.mw*(obj.w/2)^2 + 2*obj.Iwyy*(obj.w/2/obj.rw)^2 + 2*obj.Iwzz;

            A_ = zeros(3,10);
            A_(1,8)  = (-obj.Kv/obj.rw);
            A_(1,10) = (obj.Kv/obj.rw);
            A_(2,2)  = obj.mb*obj.g*obj.rb;
            A_(2,8)  = (obj.Kv);
            A_(2,10) = (-obj.Kv);
            A_(3,8) = (-obj.w*obj.Kv/2/obj.rw);
            A_(3,10) = (-obj.w*obj.Kv/2/obj.rw);
            A_ = M\A_;

            B_ = zeros(3,4);
            B_ = M\B_;

            obj.A(1:3,4:6) = eye(3);
            obj.A(4:6,:) = A_;
            obj.A(7:10,7:10) = -obj.R/obj.L*eye(4);
            obj.A(8,4) = obj.Kv/obj.rw/obj.L;
            obj.A(8,5) = -obj.Kv/obj.L;
            obj.A(8,6) = obj.w*obj.Kv/2/obj.rw/obj.L;
            obj.A(10,4) = -obj.Kv/obj.rw/obj.L;
            obj.A(10,5) = obj.Kv/obj.L;
            obj.A(10,6) = obj.w*obj.Kv/2/obj.rw/obj.L;

            obj.B(4:6,:) = B_;
            obj.B(7:10,:) = 1/obj.L*eye(4);
            
            obj.C = [eye(10)
                     zeros(4,10)
                     [0 0 0 -1/obj.rw  1 obj.w/2/obj.rw 0 0 0 0]
                     [0 0 0  1/obj.rw -1 obj.w/2/obj.rw 0 0 0 0]];

            obj.D = [zeros(10,4)
                     eye(4)
                     zeros(2,4)];
            
            % Initial conditions
            obj.x(1,:) = [0 pi/12 0 0 0 0 0 0 0 0];
            obj.t(1) = 0;
            
            % LQR Gains
            x_max    = 0.1;
            th_max   = 0.1;
            psi_max  = 0.1;
            xd_max   = 1;
            thd_max  = 1;
            psid_max = 1;
            id1_max  = 0.1;
            iq1_max  = 1;
            id2_max  = 0.1;
            iq2_max  = 1;

            vd1_max = 24;
            vq1_max = 24;
            vd2_max = 24;
            vq2_max = 24;

            Q = diag([1/x_max^2
                     1/th_max^2
                     1/psi_max^2
                     1/xd_max^2
                     1/thd_max^2
                     1/psid_max^2
                     1/id1_max^2
                     1/iq1_max^2
                     1/id2_max^2
                     1/iq2_max^2]);
            R = diag([1/vd1_max^2
                      1/vq1_max^2
                      1/vd2_max^2
                     1/vq2_max^2]);
                 
            obj.K = lqrd(obj.A, obj.B, Q, R, obj.period);
            obj.K(abs(obj.K)<1e-6)=0;
        end
        
        function [] = fsm(obj,t)
            switch obj.state
                case 0
                    obj.u(end+1,:) = -obj.K*obj.x(end,:)';
                    [~, sol]=ode45(@obj.linear_ode,[t t+obj.period],obj.x(end,:)',odeset('RelTol',1e-9));
                    obj.x(end+1,:) = sol(end,:);
                    obj.t(end+1) = t+obj.period;
                otherwise
                    warning('Invalid state in task %s',obj.name);
            end
        end
        
        function [xdot] = linear_ode(o,~,x)
            xdot = o.A*x + o.B*o.u(end,:)';
        end
        
        function [xdot] = nonlinear_ode(o,~,x)
            xdot = o.M(x)\(o.f(x) + o.b*[0.01;0]);
        end
        
        function [M] = M(o,x)
            M = [1  0  0  0                      0                       0
                 0  1  0  0                      0                       0
                 0  0  1  0                      0                       0
                 0  0  0  o.mx                   o.mb*o.rb*cos(x(2))     0
                 0  0  0  o.mb*o.rb*cos(x(2))    o.Ithe                  0
                 0  0  0  0                      0                       o.mb*o.rb^2*sin(x(2))^2 + o.Ibxx*sin(x(2))^2 + o.Ibzz*cos(x(2))^2 + o.Ipsi];
        end
        
        function [f] = f(o,x)
            f = [x(4)
                 x(5)
                 x(6)
                 o.mb*o.rb*sin(x(2))*x(5)^2
                 o.mb*o.rb*sin(x(2))*o.g + o.mb*o.rb^2*sin(x(2))*cos(x(2))*x(6)^2 + o.Ibxx*sin(x(2))*cos(x(2))*x(6)^2 - o.Ibzz*sin(x(2))*cos(x(2))
                 -2*o.mb*o.rb^2*sin(x(2))*cos(x(2))*x(5)*x(6) - 2*o.Ibxx*sin(x(2))*cos(x(2))*x(5)*x(6) + 2*o.Ibzz*sin(x(2))*cos(x(2))*x(5)*x(6)];
        end
        
        function [b] = b(o)
            b = [0              0
                 0              0
                 0              0
                 1/o.rw         1/o.rw
                 -1             -1
                 o.w/2/o.rw     -o.w/2/o.rw];
        end
    end
end