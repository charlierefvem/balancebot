dt = 1e-3;
TL = TaskList(dt);

T1 = Task1('Motor', 1e-3);
TL.AddTask(T1);

steps = 10000;

fprintf('Running Simulation from 0 to %2.4f seconds...\n',dt*(steps-1));
tic;
TL.loop(steps);
fprintf('Simulation finished after %2.4f seconds of execution.\n',toc);

%%
figtitles = {'x'
           'theta'
           'psi'
           'x_dot'
           'theta_dot'
           'psi_dot'
           'id1'
           'iq1'
           'id2'
           'iq2'
           'vd1'
           'vq1'
           'vd2'
           'vq2'};
ylabels = {'Linear Displacement [m]'
           'Pitch Angle [rad]'
           'Yaw Angle [rad]'
           'Linear Velocity [m/s]'
           'Pitch Velocity [rad/s]'
           'Yaw Velocity [rad/s]'
           'M1 Direct Current [A]'
           'M1 Quadrature Current [A]'
           'M2 Direct Current [A]'
           'M2 Quadrature Current [A]'
           'M1 Direct Voltage [V]'
           'M1 Quadrature Voltage [V]'
           'M2 Direct Voltage [V]'
           'M2 Quadrature Voltage [V]'};
for n=1:10
    hf(n)=figure(n);
    set(hf(n),'Name',figtitles{n});
    plot(T1.t,T1.x(:,n));
    xlabel('Time [s]');
    ylabel(ylabels{n});
end

for m=1:4
    hf(n+m)=figure(n+m);
    set(hf(n+m),'Name',figtitles{n+m});
    plot(T1.t(1:end-1),T1.u(:,m));
    xlabel('Time [s]');
    ylabel(ylabels{n+m});
end