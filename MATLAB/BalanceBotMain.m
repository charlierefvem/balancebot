%% Define parameters and initial conditions
P = struct('m1', 2*(25e-3+150e-3),...
           'm2', 2*(150e-3)+400e-3,...
           'I1', 2*(50e-6+24e-6),...
           'I2', 1000e-6,...
           'r1', (90e-3/2),...
           'r2', 75e-3,...
           'g',  9.81,...
           'b1', 1e-3,...
           'b2', 1e-4);
P.m = P.m1+P.m2+P.I1/(P.r1^2);
P.I = P.m2*P.r2^2+P.I2;

load_system('BalanceBotClosedLoopObserver');
mdlWks = get_param('BalanceBotClosedLoopObserver', 'ModelWorkspace');
assignin(mdlWks,'P',P);

x0 = [0;0;0;pi/12];

tstep = 0.075;
tfinal = 40;

reference = (0:tstep/10:tfinal)';
reference(:,2) = 0;
reference(:,3) = 0;
reference(:,4) = -1;
reference(mod(floor(reference(:,1)/8),2)==0,4) = 1;
reference(:,5) = 0;


M = [ P.m       -P.m2*P.r2   0   0
     -P.m2*P.r2  P.I         0   0
      0          0           1   0
      0          0           0   1 ];

A = [-(P.b2/P.r1^2+P.b1) -P.b2/P.r1   0   0
     -P.b2/P.r1          -P.b2        0   P.m2*P.r2*P.g
      1                   0           0   0
      0                   1           0   0 ];

B = [ 1/P.r1
      1
      0
      0 ];

A = M\A;
B = M\B;

C = [1 0 0 0
     0 1 0 0
     0 0 1 0
     0 0 0 1];

% C = [ 1/P.r1    1       0       0
%       0         1       0       0
%       0         0       1/P.r1  1
%       0         0       0       1 ];

D = [0
     0
     0
     0];

Q = diag([1/0.003^2    1/0.003^2   1/0.003^2   1/0.020^2]);
R = 1/(0.50^2);
 
[K,~,poles] = lqr(A,B,Q,R);




N = K/(C-D*K);

%% Run the simulation
sim('BalanceBotClosedLoopObserver',tfinal);

%% Make Plots
figure(1);
% subplot(2,1,1)
%     plot(tout,r(:,3),t,y(:,3));
%     legend('Reference','Output');
%     xlabel('Time [s]');
%     ylabel('Linear Position [m]');
% subplot(2,1,2)
    plot(tout,r(:,4),t,y(:,4));
    legend('Reference','Output');
    xlabel('Time [s]');
    ylabel('Balance Bot Angle [rad]');

figure(2);
plot(tout,u);
xlabel('Time [s]');
ylabel('Torque [Nm]');

%% Animate
Base = P.r1*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
             -1               0];
        
Body = [-.5*P.r1 0
        .5*P.r1  0
        .5*P.r1  2*P.r2
        -.5*P.r1 2*P.r2];
    
BaseRotated = zeros(size(Base,1),size(Base,2),length(tout));
BodyRotated = zeros(size(Body,1),size(Body,2),length(tout));

tic
for n = 1:length(tout)
    R1 = [cos(-x(n,3)/P.r1) sin(-x(n,3)/P.r1)
         -sin(-x(n,3)/P.r1) cos(-x(n,3)/P.r1)];
    R2 = [cos(x(n,4)) sin(x(n,4))
         -sin(x(n,4)) cos(x(n,4))];
    BaseRotated(:,:,n) = Base*R1 + [1 0]*x(n,3);
    BodyRotated(:,:,n) = Body*R2 + [1 0]*x(n,3);
end
hf4=figure(4);
for n=1:length(tout)
    figure(hf4);
    plot([-5 5],[-P.r1 -P.r1],'linewidth',2);
    hold on;
    plot([r(n,3) r(n,3)], (P.r1+2*P.r2)*[-1 1]);
    fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
    fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
    hold off;
    axis equal;
%     xlim((P.r1+P.r2)*[-5 5]);
    xlim([-1.2 1.2]);
    ylim((P.r1+2*P.r2)*[-1 1]);
    grid on;
    drawnow;
%     frame = getframe(hf4);
%     im{n} = frame2im(frame);
end
toc
%%
% filename = 'BalanceBotControlled.gif';
% for n = 1:length(tout)
%     [A,map] = rgb2ind(im{n},256);
%     if n == 1
%         imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',tstep);
%     else
%         imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',tstep);
%     end
% end