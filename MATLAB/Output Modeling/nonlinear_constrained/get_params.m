function [p] = get_params()
%% System Parameters

% Geometry
p.rw  = 45e-3;                                              % [m]
p.rb  = 25e-3;                                              % [m]
p.w   = 120e-3;                                             % [m]

% Gravity
p.g   = 9.81;                                               % [m/s/s]

% Masses
p.mw  = 80e-3;                                              % [kg]
p.mb  = 800e-3;                                             % [kg]

% Wheel inertias
p.Iw  = 2.5e-6;                                             % [kg*m^2]
p.Iwxx= 0.5*p.Iw;                                           % [kg*m^2]
p.Iwyy= p.Iw;                                               % [kg*m^2]
p.Iwzz= 0.5*p.Iw;                                           % [kg*m^2]

% Body inertias
p.Ib  = 5.8e-3;                                             % [kg*m^2]
p.Ibxx= p.Ib;                                               % [kg*m^2]
p.Ibyy= p.Ib;                                               % [kg*m^2]
p.Ibzz= 0.5*p.Ib;                                           % [kg*m^2]

% Effective Masses and Inertias
p.mx  = p.mb+2*p.mw+2*p.Iw/(p.rw^2);                        % [kg]
p.Ith = p.mb*p.rb^2 + p.Ib;                                 % [kg*m^2]
p.Ips = p.mw*p.w^2/2 + p.Iwyy/p.rw^2*p.w^2/2 + 2*p.Iwzz;    % [kg*m^2]
end