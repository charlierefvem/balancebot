%% EE513 Term Project Simulation - Self Balancing Robot
%
% *Author* Charlie Refvem
%
% *Date* Winter 2019
%
% *Description*
%
% This script is meant to suplement the report and presentation for EE513
% in Winter 2019. This script defines all of the mass parameters for the
% model and computes the feedback gain and then runs a Simulink(R) file to
% perform the simulation.
%
% After the simulation runs, this script will process the results and
% generate an animation showing the balance bot tracking a position
% reference.
%
% *Dependencies*
%
% * |BalanceBotClosedLoop.slx| - Simulink(R) model used to simulate the
%                                system response.
% * |BalanceBot.jpg|           - Icon for the masked subsystem representing
%                                the balance bot. 


%% Define parameters and initial conditions
% All of the mechanical properties are stored in a struct to make it easy
% to share the data with multiple functions in the Simulink(R) model.
P = struct('m1', 2*(25e-3+150e-3),...
           'm2', 2*(150e-3)+400e-3,...
           'I1', 2*(50e-6+24e-6),...
           'I2', 1000e-6,...
           'r1', (90e-3/2),...
           'r2', 75e-3,...
           'g',  9.81,...
           'b1', 1e-3,...
           'b2', 1e-4);
P.m = P.m1+P.m2+P.I1/(P.r1^2);
P.I = P.m2*P.r2^2+P.I2;
%%
% The data is then assigned to the simultion workspace so that the
% functions in the Simulink(R) model have access to the properties.
load_system('BalanceBotClosedLoop');
mdlWks = get_param('BalanceBotClosedLoop', 'ModelWorkspace');
assignin(mdlWks,'P',P);
%%
% Initial conditions are set so that the balance bot is off balance by 30
% degrees.
x0 = [0;0;0;pi/12];

%% Generate reference data
% The controller will attempt to drive all states to zero except for the
% linear position which is meant to track a square wave input.

tstep = 0.075;
tfinal = 40;

reference = (0:tstep/10:tfinal)';
reference(:,2) = 0;
reference(:,3) = 0;
reference(:,4) = -1;
reference(mod(floor(reference(:,1)/8),2)==0,4) = 1;
reference(:,5) = 0;

%% Model linearization
% The following matrices are taken from Equation 35 as labeled in the
% report and are used to compute the A and B matrices used for gain
% determination.
%
% $M$, $A$, and $B$, all result from linearizing the system about an
% operting point where all of the states are zero.
%
% $C$, and $D$ are set to trivial values so that all of the states may be
% analysed and plotted after the simulation.

M = [ P.m       -P.m2*P.r2   0   0
     -P.m2*P.r2  P.I         0   0
      0          0           1   0
      0          0           0   1 ];

A = [-(P.b2/P.r1^2+P.b1) -P.b2/P.r1   0   0
     -P.b2/P.r1          -P.b2        0   P.m2*P.r2*P.g
      1                   0           0   0
      0                   1           0   0 ];

B = [ 1/P.r1
      1
      0
      0 ];

A = M\A;
B = M\B;

C = [1 0 0 0
     0 1 0 0
     0 0 1 0
     0 0 0 1];

D = [0
     0
     0
     0];

%% Feedback controller design
% This section computes the feedback gain $K$ using LQR. THe $Q$ and $R$
% matrices were selected using Bryson's rule and then adjusted to produce
% the desired system response without actutor saturation.
%
% The matrix $N$ is used to rescale the reference input so that the
% tracking controller yields zero steady-state error.
 
Q = diag([1/0.003^2    1/0.003^2   1/0.003^2   1/0.020^2]);
R = 1/(0.50^2);
 
[K,~,P] = lqr(A,B,Q,R);
[Kd,~,Pd] = lqrd(A,B,Q,R,1/100);

N = K/(C-D*K);
Nd = Kd/(C-D*Kd);

%% Run the simulation
sim('BalanceBotClosedLoop',tfinal);

%% Make Plots
% Here some plots are generated.
% 
% The first plot shows the simulation
% results for the third and fourth states in the system: the linear
% position of the balance bot and the angle of the balance bot.
%
% The second plot shows the torque input to the system required to balance
% the bot and track the reference.
figure(1);
subplot(2,1,1)
    plot(tout,r(:,3),t,y(:,3));
    legend('Reference','Output');
    xlabel('Time [s]');
    ylabel('Linear Position [m]');
subplot(2,1,2)
    plot(tout,r(:,4),t,y(:,4));
    legend('Reference','Output');
    xlabel('Time [s]');
    ylabel('Balance Bot Angle [rad]');

figure(2);
plot(tout,u);
xlabel('Time [s]');
ylabel('Torque [Nm]');

%% Animate
% This section produces an animation using a series of plots and then
% stores animation frames in an animated GIF file.
%
% Set the variable |anim| to false to prevent the animation from running
% and set the variable |makegif| to false to prevent the GIF from being
% saved to the Current Folder. Note that it make take up to several minutes
% to save the animation to the GIF depending on your computer and plot
% resolution.
%

anim = true;
makegif = false;

if(anim)
    Base = P.r1*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
                 -1               0];

    Body = [-.5*P.r1 0
            .5*P.r1  0
            .5*P.r1  2*P.r2
            -.5*P.r1 2*P.r2];

    BaseRotated = zeros(size(Base,1),size(Base,2),length(tout));
    BodyRotated = zeros(size(Body,1),size(Body,2),length(tout));

    for n = 1:length(tout)
        R1 = [cos(-x(n,3)/P.r1) sin(-x(n,3)/P.r1)
             -sin(-x(n,3)/P.r1) cos(-x(n,3)/P.r1)];
        R2 = [cos(x(n,4)) sin(x(n,4))
             -sin(x(n,4)) cos(x(n,4))];
        BaseRotated(:,:,n) = Base*R1 + [1 0]*x(n,3);
        BodyRotated(:,:,n) = Body*R2 + [1 0]*x(n,3);
    end
    hf4=figure(4);
    for n=1:length(tout)
        figure(hf4);
        plot([-5 5],[-P.r1 -P.r1],'linewidth',2);
        hold on;
        plot([r(n,3) r(n,3)], (P.r1+2*P.r2)*[-1 1]);
        fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
        fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
        hold off;
        axis equal;
        xlim([-1.2 1.2]);
        xticks([]);
        xticklabels({})
        ylim((P.r1+2*P.r2)*[-1 1]);
        yticks([]);
        yticklabels({})
        drawnow;
        if makegif
            frame = getframe(hf4);
            im{n} = frame2im(frame);
        end
    end

    if makegif
        filename = 'BalanceBotControlled.gif';
        for n = 1:length(tout)
            [A,map] = rgb2ind(im{n},256);
            if n == 1
                imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',tstep);
            else
                imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',tstep);
            end
        end
    end
end