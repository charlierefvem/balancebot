function x_k = f1(x_km,u)
Ts = 1/250;

m1=2*(25e-3+150e-3);
global P
m2=2*(150e-3)+400e-3;
I1=2*(50e-6+24e-6);
I2=1000e-6;
r1=90e-3/2;
r2=75e-3;
g=9.81;
b1=1e-3;
b2=1e-4;
m = m1+m2+I1/(r1^2);
I = m2*r2^2+I2;

% K = P.K;
% N = P.N;
% u= N*r-K*x_km;
% if u>0.5
%     u=0.5;
% elseif u<-0.5
%     u=-0.5;
% end


f = [-(m2*r2*x_km(2)^2*sin(x_km(4)) -1/(r1)*b2*(x_km(1)/r1+x_km(2)) -b1*(x_km(1)/r1+x_km(2)))
     (m2*g*r2*sin(x_km(4))    -b2*(x_km(1)/r1+x_km(2)))
      x_km(1)
      x_km(2)];
  
g =  [ 1/r1
       1
       0
       0 ];

m =  [   m                    -m2*r2*cos(x_km(4))     0       0
        -m2*r2*cos(x_km(4))     I                     0       0
         0                       0                       1       0
         0                       0                       0       1 ];
         
x_k=x_km+ Ts*(m\(f+g*u));

  