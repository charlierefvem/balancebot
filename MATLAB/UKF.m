function [xh_k_k, P_k_k] = UKF(u, z_k, xh_km_km, P_km_km, P)
%%
% Sample time
Ts = 1/250;

% Number of states
n = length(xh_km_km);

% Process noise covariance
Q_k = [Ts^3/3 Ts^2/2  0       0
       Ts^2/2 Ts      0       0
       0      0       Ts^3/3  Ts^2/2
       0      0       Ts^2/2  Ts    ];
% Process noise mean value
w_k = [0
       0
       0
       0];

% Measurement noise covariance
R = [5   0   0   0
     0   25  0   0
     0   0   2   0
     0   0   0   5];
% Measurement noise mean value
v_k = [0
       0
       0
       0];

%% State to state and input to state coupling functions
f = @(x) (Ts*[-(P.m2*P.r2*x(2)^2*sin(x(4)) -1/(P.r1)*P.b2*(x(1)/P.r1+x(2)) -P.b1*(x(1)/P.r1+x(2)))
               (P.m2*P.g*P.r2*sin(x(4))    -P.b2*(x(1)/P.r1+x(2)))
                x(1)
                x(2)]);
  
g = @(x) Ts*[ 1/P.r1
              1
              0
              0 ];

m = @(x) [   P.m                    -P.m2*P.r2*cos(x(4))     0       0
            -P.m2*P.r2*cos(x(4))     P.I                     0       0
             0                       0                       1       0
             0                       0                       0       1 ];

C = [ 1/P.r1    1       0       0
      0         1       0       0
      0         0       1/P.r1  1
      0         0       0       1 ];

D = [0
     0
     0
     0];

%% Predict
% Augmented state vector
xa_km_km = [xh_km_km;
            w_k];
% Augmented covariance matrix
Pa_km_km = [P_km_km   zeros(n)
            zeros(n)  Q_k     ];
% Number of states in system
L = 2*n;
alp = 1e-3;
bet = 2;
kap = 0;
lam = alp^2*(L+kap)-L;

% Generate sigma points
chi_km_km = zeros(L,2*L+1);
chi_km_km(:,  1)       = xa_km_km;
chi_km_km(:,  2:L+1)   = xa_km_km*ones(1,L)+real(sqrtm((L+lam)*Pa_km_km));
chi_km_km(:,L+2:2*L+1) = xa_km_km*ones(1,L)-real(sqrtm((L+lam)*Pa_km_km));

% Propagate sigma points
chi_k_km = zeros(n,2*L+1);
for ii = 1:2*L+1
    chi_k_km(:,ii) = chi_km_km(  1:n,ii)+...
                     chi_km_km(n+1:L,ii)+...
                     m(chi_km_km(1:n,ii))\(f(chi_km_km(1:n,ii))+g(chi_km_km(1:n,ii))*u);
end

% Compute weights for sigma points
ws = [lam/(L+lam) 1/2/(L+lam)*ones(1,2*L)];
wc = [lam/(L+lam)+(1-alp^2+bet) 1/2/(L+lam)*ones(1,2*L)];

% Average sigma points
x_k_km  = chi_k_km*ws';
P_k_km = zeros(n);
for ii = 1:2*L+1
    P_k_km = P_k_km + wc(ii)*(chi_k_km(:,ii) - x_k_km)*(chi_k_km(:,ii) - xh_km_km).';
end

%% Update
% Augmented state vector
xa_k_km = [x_k_km
           v_k];
% Augmented state covariance matrix
Pa_k_km = [P_k_km     zeros(n)
           zeros(n)   R];

% Generate sigma points
chi_k_km = zeros(L,2*L+1);
chi_k_km(:,  1)       = xa_k_km;
chi_k_km(:,  2:L+1)   = xa_k_km*ones(1,L)+real(sqrtm((L+lam)*Pa_k_km));
chi_k_km(:,L+2:2*L+1) = xa_k_km*ones(1,L)-real(sqrtm((L+lam)*Pa_k_km));

% Propagate sigma points
gam_k=zeros(n,2*L+1);
for ii = 1:2*L+1
    gam_k(:,ii) = C*chi_k_km(1:n,ii)+D*u+chi_k_km(n+1:L,ii);
end
zh_k  = gam_k*ws';
Pzz_k = zeros(n,n);
Pxz_k = zeros(n,n);
for ii = 1:2*L+1
    Pzz_k = Pzz_k + wc(ii)*(gam_k(:,ii)      - zh_k)*(gam_k(:,ii) - zh_k).';
    Pxz_k = Pxz_k + wc(ii)*(chi_k_km(1:n,ii) - x_k_km)*(gam_k(:,ii) - zh_k).';
end

% Compute kalman gain
K_k = Pxz_k/Pzz_k;

% Finish estimate
xh_k_k = x_k_km+K_k*(z_k - zh_k);
P_k_k = P_k_km - K_k*Pxz_k.'-Pxz_k*K_k.' + K_k*Pzz_k*K_k.';