%% Symbolic EOM Derivation

%% Declare symbolic variables and functions
syms m m_w I_xx I_yy I_zz I_w
syms r w r_w
syms T_r T_l F_r F_l
syms theta(t) psi(t) x(t)
syms g

%% Moment of inertia tensor calculation
Ibod = diag([I_xx I_yy I_zz]);
% R transforms from xyz frame to body frame
% R' transforms from body frame to xyz frame
R = [cos(theta) 0 -sin(theta)
     0          1  0
     sin(theta) 0  cos(theta)];

I = R.'*Ibod*R;

%% Position of centers of mass
r_g = [r*sin(theta); 0; r*cos(theta)];
r_r = [0; -w/2; 0];
r_l = [0; w/2; 0];

%% Angular Velocity
Omega     = [0; 0;             diff(psi,t)];
Omega_dot = [0; 0;             diff(psi,t,t)];
omega_y = diff(theta,t);
omega_z = diff(psi,t);
omega     = [0; diff(theta,t); diff(psi,t)];

%% Linear velocity of centers of mass
v_c   = [diff(x,t); 0; 0];

v_g_rel = cross([0; diff(theta,t); 0],r_g);
v_g     = v_c + cross(Omega,r_g) + v_g_rel;

v_r_rel = cross([0; diff(theta,t); 0],r_r);
v_r     = v_c + cross(Omega,r_r) + v_r_rel;

v_l_rel = cross([0; diff(theta,t); 0],r_l);
v_l     = v_c + cross(Omega,r_l) + v_l_rel;

%% Linear Acceleration of centers of mass
a_c   = [diff(x,t,t);0;0]+cross(Omega,v_c);
a_g_rel = cross([0;diff(theta,t);0],cross([0;diff(theta,t);0],r_g))...
        + cross([0;diff(theta,t,t);0],r_g);
a_g     = a_c...
        + cross(Omega_dot,r_g)...
        + cross(Omega,cross(Omega,r_g))...
        + cross(2*Omega,v_g_rel)...
        + a_g_rel;
    
a_r_rel = cross([0;diff(theta,t);0],cross([0;diff(theta,t);0],r_r))...
        + cross([0;diff(theta,t,t);0],r_r);
a_r     = a_c...
        + cross(Omega_dot,r_r)...
        + cross(Omega,cross(Omega,r_r))...
        + cross(2*Omega,v_r_rel)...
        + a_r_rel;
    
a_l_rel = cross([0;diff(theta,t);0],cross([0;diff(theta,t);0],r_l))...
        + cross([0;diff(theta,t,t);0],r_l);
a_l     = a_c...
        + cross(Omega_dot,r_l)...
        + cross(Omega,cross(Omega,r_l))...
        + cross(2*Omega,v_l_rel)...
        + a_l_rel;

%% Angular momentum
Hg      = I*omega;
Hc      = Hg + cross(r_g,m*(v_g));
Hc_dot = diff(Hc,t)+cross(Omega,Hc);

%% Sum moments on wheels to get forces at axels
alpha_r = ([1 0 0]*a_r)/r_w;
M_r = [0 1 0]*([0; T_r; 0] +  cross([0; 0; r_w],[-F_r;0;0]));
F_r = rhs(isolate((M_r == (I_w + m_w*r_w^2)*alpha_r),F_r));

alpha_l = ([1 0 0]*a_l)/r_w;
M_l = [0 1 0]*([0; T_l; 0] +  cross([0; 0; r_w],[-F_l;0;0]));
F_l = rhs(isolate((M_l == (I_w + m_w*r_w^2)*alpha_l),F_l));

%% Sum forces on body
eq1 = F_l+F_r == m*[1 0 0]*a_g;

%% Sum moments on body
eq2 = m*g*r*sin(theta) - T_l - T_r == [0 1 0]*Hc_dot;
eq3 = w/2*(F_r - F_l) == [0 0 1]*Hc_dot;

sfun = [diff(x,t,t) diff(theta,t,t) diff(psi,t,t) diff(x,t) diff(theta,t) diff(psi,t) x theta psi];
syms x1dd x2dd x3dd x1d x2d x3d x1 x2 x3;
svar = [x1dd x2dd x3dd x1d x2d x3d x1 x2 x3];

eq1 = subs(eq1,sfun,svar);
eq2 = subs(eq2,sfun,svar);
eq3 = subs(eq3,sfun,svar);
str1=latex(eq1);
str2=latex(eq2);
str3=latex(eq3);

xdd = solve([eq1 eq2 eq3], [x1dd x2dd x3dd]);
odes = [xdd.x1dd;xdd.x2dd;xdd.x3dd];
AB = jacobian(odes,[x1, x2, x3, x1d, x2d, x3d, T_l, T_r]);
AB_lin = subs(AB,[x1,x2,x3,x1d,x2d,x3d],[0, 0, 0, 0, 0, 0]);
% latex(xdd.x1dd)
% latex(xdd.x2dd)
% latex(xdd.x3dd)
%%
out = fopen('output.tex','w');
fwrite(out,fileread('header.tex'));
fprintf(out,'%s\n','\section{Nonlinear Equations}');
fprintf(out,'%s\n','\tiny');
fprintf(out,'%s\n','\begin{align}');
fprintf(out,'%s\\\\[20pt]\n',str1);
fprintf(out,'%s\\\\[20pt]\n',str2);
fprintf(out,'%s\n',str3);
fprintf(out,'%s\n','\end{align}');
fprintf(out,'%s\n','\normalsize');
fprintf(out,'%s\n','\section{Linearized Equations}');
fprintf(out,'%s\n','\begin{align}');
fprintf(out,'%s\n',latex(AB_lin));
fprintf(out,'%s\n','\end{align}');
fwrite(out,fileread('footer.tex'));
fclose(out);