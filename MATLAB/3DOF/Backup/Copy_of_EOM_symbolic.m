%% Symbolic EOM Derivation

%% Declare symbolic variables and functions
syms m r I_xx I_yy I_zz
syms theta(t) psi(t) x(t)

%% Moment of inertia tensor calculation
Ibod = diag([I_xx I_yy I_zz]);
% R transforms from xyz frame to body frame
% R' transforms from body frame to xyz frame
R = [cos(theta) 0 -sin(theta)
     0          1  0
     sin(theta) 0  cos(theta)];

I = R.'*Ibod*R;

%% Position
r_g = [r*sin(theta); 0; r*cos(theta)];

%% Angular Velocity
Omega     = [0; 0;             diff(psi,t)];
Omega_dot = [0; 0;             diff(psi,t,t)];
omega_y = diff(theta,t);
omega_z = diff(psi,t);
omega     = [0; diff(theta,t); diff(psi,t)];

%% Linear velocity
v_c   = [diff(x,t); 0; 0];
v_rel = cross([0; diff(theta,t); 0],r_g);
v_g   = v_c + cross(Omega,r_g) + v_rel;

%% Linear Acceleration
a_c   = [diff(x,t,t);0;0]+cross(Omega,v_c);
a_rel = cross([0;diff(theta,t);0],cross([0;diff(theta,t);0],r_g))...
      + cross([0;diff(theta,t,t);0],r_g);
a_g   = a_c...
      + cross(Omega_dot,r_g)...
      + cross(Omega,cross(Omega,r_g))...
      + cross(2*Omega,v_rel)...
      + a_rel;

%% Angular momentum
Hg      = I*omega;
Hg_dot  = diff(Hg,t)+cross(Omega,Hg);
Hc_dot  = Hg_dot  + cross(r_g,m*(a_g)); %<-------------

Hc      = I*omega + cross(r_g,m*(v_g));
Hc_dot2 = diff(Hc,t)+cross(Omega,Hc);   %<-------------

simplify(Hc_dot-Hc_dot2)

%% Convert to LaTeX strings
Hc_dot_x = latex(simplify(simplify([1 0 0]*Hc_dot)));
Hc_dot_y = latex(simplify(simplify([0 1 0]*Hc_dot)));
Hc_dot_z = latex(simplify(simplify([0 0 1]*Hc_dot)));
lat_str = sprintf('\\begin{align}\n\\dot{H}_x&=%s\\\\[10pt]\n\\dot{H}_y&=%s\\\\[10pt]\n\\dot{H}_z&=%s\\end{align}',Hc_dot_x,Hc_dot_y,Hc_dot_z);

%% Sum torques


% %% Jacobian computation
% syms x1 x2 x3 xd1 xd2 xd3 xdd1 xdd2 xdd3
% Hcd = subs(Hc_dot,[diff(x,t,t) diff(theta,t,t) diff(psi,t,t) diff(x,t) diff(theta,t) diff(psi,t) x theta psi],[xdd1 xdd2 xdd3 xd1 xd2 xd3 x1 x2 x3])
% solve(Hcd,[xdd1 xdd2 xdd3])