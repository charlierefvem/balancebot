g   = 9.81;         % Acceleration from gravity, [m/s/s]
m   = 0.25;         % Mass of body, [kg]
m_w = 0.075;        % Mass of wheel and rotor, [kg]
r   = 0.10;         % Distance from center of wheel to cg of body, [m]
r_w = 0.045;        % Wheel radius, [m]
w   = 0.10;         % Distance between wheels, [m]
I_xx = 1/4*m*r^2;   % Inertia of body in body x axis, [kg*m*m]
I_yy = 1/4*m*r^2;   % Inertia of body in body y axis, [kg*m*m]
I_zz = 1/24*m*w^2;  % Inertia of body in body z axis, [kg*m*m]
I_w  = 1/2*m*r^2;   % Inertia of wheel, [kg*m*m]
b1 = 1e-3;
b2 = 1e-3;
b3 = 1e-3;



Ao = [ 0,                                                                                                                                  0, 0,  1,   0,   0
       0,                                                                                                                                  0, 0,  0,   1,   0
       0,                                                                                                                                  0, 0,  0,   0,   1
       0,                                -(g*m^2*r^2*r_w^2)/(2*I_w*I_yy + 2*I_w*m*r^2 + I_yy*m*r_w^2 + 2*I_yy*m_w*r_w^2 + 2*m*m_w*r^2*r_w^2), 0, -b1,  0,   0
       0, (g*r*m^2*r_w^2 + 2*g*m_w*r*m*r_w^2 + 2*I_w*g*r*m)/(2*I_w*I_yy + 2*I_w*m*r^2 + I_yy*m*r_w^2 + 2*I_yy*m_w*r_w^2 + 2*m*m_w*r^2*r_w^2), 0,  0,  -b2,  0
       0,                                                                                                                                  0, 0,  0,   0,  -b3 ];

Bo = [ 0,                                                                                                                           0
       0,                                                                                                                           0
       0,                                                                                                                           0                                                                                                                           
       (r_w*(m*r^2 + m*r_w*r + I_yy))/(2*I_w*I_yy + 2*I_w*m*r^2 + I_yy*m*r_w^2 + 2*I_yy*m_w*r_w^2 + 2*m*m_w*r^2*r_w^2),             (r_w*(m*r^2 + m*r_w*r + I_yy))/(2*I_w*I_yy + 2*I_w*m*r^2 + I_yy*m*r_w^2 + 2*I_yy*m_w*r_w^2 + 2*m*m_w*r^2*r_w^2)
       -(2*I_w + m*r_w^2 + 2*m_w*r_w^2 + m*r*r_w)/(2*I_w*I_yy + 2*I_w*m*r^2 + I_yy*m*r_w^2 + 2*I_yy*m_w*r_w^2 + 2*m*m_w*r^2*r_w^2), -(2*I_w + m*r_w^2 + 2*m_w*r_w^2 + m*r*r_w)/(2*I_w*I_yy + 2*I_w*m*r^2 + I_yy*m*r_w^2 + 2*I_yy*m_w*r_w^2 + 2*m*m_w*r^2*r_w^2)
       -(r_w*w)/(m_w*r_w^2*w^2 + 2*I_zz*r_w^2 + I_w*w^2),                                                                           (r_w*w)/(m_w*r_w^2*w^2 + 2*I_zz*r_w^2 + I_w*w^2)                                                                           ];

Co = eye(6);

Do = zeros(6,2);

%       x           theta       psi         xd          thetad      psid
Q = [   1/0.1^2     0           0           0           0           0
        0           1/0.1^2     0           0           0           0
        0           0           1/1^2       0           0           0
        0           0           0           1/0.1^2     0           0
        0           0           0           0           1/0.5^2     0
        0           0           0           0           0           1/1^2];
    

R = [1/0.5^2  0
     0       1/0.5^2];
 
N = zeros(6,2);

K = lqr(Ao,Bo,Q,R,N);

Ac = Ao-Bo*K;
Bc = zeros(6,1);
Cc = [Co
      -K];
Dc = 0;

cl_sys = ss(Ac,Bc,Cc,Dc);

x0 = [-1 pi/4 0 0 0 0]';

t = (0:0.05:10)';
u = zeros(length(t),1);

[y,t] = lsim(cl_sys,u,t,x0);

%% Animate
Wheel = r_w*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
             -1                 0];
        
Body = [-.5*r 0
         .5*r 0
         .5*r 2*r
        -.5*r 2*r];
    
BaseRotated = zeros(size(Wheel,1),size(Wheel,2),length(t));
BodyRotated = zeros(size(Body,1),size(Body,2),length(t));
x  = y(:,1);
th = y(:,2);

for n = 1:length(t)
    R1 = [cos(x(n)/r_w) sin(x(n)/r_w)
         -sin(x(n)/r_w) cos(x(n)/r_w)];
    R2 = [cos(th(n)) sin(th(n))
         -sin(th(n)) cos(th(n))];
    BaseRotated(:,:,n) = Wheel*R1' + [1 0]*x(n);
    BodyRotated(:,:,n) = Body*R2' + [1 0]*x(n);
end

tic
hf4=figure(4);
for n=1:length(t)
    figure(hf4);
    plot([-5 5],[-r_w -r_w],'linewidth',2);
    hold on;
    fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
    fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
    hold off;
    axis equal;
    %xlim((P.r1+P.r2)*[-5 5]);
    xlim([-1.2 1.2]);
    ylim((2.5*r)*[-1 1]);
    grid on;
    drawnow;
end
toc