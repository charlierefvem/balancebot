%%
mb   = 0.7;     % [kg] mass of body
Ibxx = 10e-4;   % [kg*m^2] centroidal inertia about x of body
Ibyy = 10e-4;   % [kg*m^2] centroidal inertia about y of body
Ibzz = 5e-4;    % [kg*m^2] centroidal inertia about z of body

mw   = 0.175;   % [kg] mass of wheel
Iwxx = 0.5e-4;  % [kg*m^2] centroidal inertia about x of wheel
Iwyy = 1.0e-4;  % [kg*m^2] centroidal inertia about y of wheel
Iwzz = 0.5e-4;  % [kg*m^2] centroidal inertia about z of wheel

r    = 45e-3;   % [m] radius of wheel
d    = 60e-3;   % [m] distance from axle to CG of body
w    = 115e-3;  % [m] distance between CG of wheels on axle

g    = 9.81;    % [m/s/s] acceleration due to gravity

Kv   = 0.043;   % [Nm/A] motor torque constant
R    = 1.39;    % [ohm] winding resistance
L    = 1.18e-3; % [H] winding inductance

%%
M = zeros(3);
M(1,1) = mb + 2*mw + 2*Iwyy/r^2;
M(1,2) = mb*d;
M(2,1) = mb*d;
M(2,2) = mb*d^2 + Ibyy;
M(3,3) = Ibzz + 2*mw*(w/2)^2 + 2*Iwyy*(w/2/r)^2 + 2*Iwzz;

A_ = zeros(3,10);
A_(1,8)  = (-Kv/r);
A_(1,10) = (Kv/r);
A_(2,2)  = mb*g*d;
A_(2,8)  = (Kv);
A_(2,10) = (-Kv);
A_(3,8) = (-w*Kv/2/r);
A_(3,10) = (-w*Kv/2/r);
A_ = M\A_;

B_ = zeros(3,4);
B_ = M\B_;

A = zeros(10);
A(1:3,4:6) = eye(3);
A(4:6,:) = A_;
A(7:10,7:10) = -R/L*eye(4);
A(8,4) = Kv/r/L;
A(8,5) = -Kv/L;
A(8,6) = w*Kv/2/r/L;
A(10,4) = -Kv/r/L;
A(10,5) = Kv/L;
A(10,6) = w*Kv/2/r/L;

B = zeros(10,4);
B(4:6,:) = B_;
B(7:10,:) = 1/L*eye(4);

C = [eye(10)
     zeros(4,10)
     [0 0 0 -1/r  1 w/2/r 0 0 0 0]
     [0 0 0  1/r -1 w/2/r 0 0 0 0]];
 
D = [zeros(10,4)
     eye(4)
     zeros(2,4)];

x_max    = 0.1;
th_max   = 0.1;
psi_max  = 0.1;
xd_max   = 1;
thd_max  = 1;
psid_max = 1;
id1_max  = 0.1;
iq1_max  = 1;
id2_max  = 0.1;
iq2_max  = 1;

vd1_max = 24;
vq1_max = 24;
vd2_max = 24;
vq2_max = 24;

lqr_data.Q = diag([1/x_max^2
                   1/th_max^2
                   1/psi_max^2
                   1/xd_max^2
                   1/thd_max^2
                   1/psid_max^2
                   1/id1_max^2
                   1/iq1_max^2
                   1/id2_max^2
                   1/iq2_max^2]);
lqr_data.R = diag([1/vd1_max^2
                   1/vq1_max^2
                   1/vd2_max^2
                   1/vq1_max^2]);
%lqr_data.K = lqr(A,B,lqr_data.Q, lqr_data.R);
lqr_data.K = lqrd(A,B,lqr_data.Q, lqr_data.R, 1/1000);
lqr_data.K(abs(lqr_data.K)<1e-6)=0;

%%
x0 = [  0       % x
        0       % th
        0       % psi
        0       % xd
        0       % thd
        0       % psid
        0       % id1
        0       % iq1
        0       % id2
        0 ];    % iq2

    
%% Open Loop
K = zeros(4,10);
sim('full_model',1);

%%
hf1=figure(1);
set(hf1,'Name','OL: x'); 
plot(tout,simout(:,1));
xlabel('Time [s]');
ylabel('Horizontal Displacement [m]');

hf2=figure(2);
set(hf2,'Name','OL: th');
plot(tout,simout(:,2));
xlabel('Time [s]');
ylabel('Pitch Angle [rad]');

hf3=figure(3);
set(hf3,'Name','OL: psi');
plot(tout,simout(:,3));
xlabel('Time [s]');
ylabel('Yaw Angle [rad]');


%% Closed Loop
K = lqr_data.K;
sim('full_model',10);

%%
hf4=figure(4);
set(hf4,'Name','CL: x');
plot(tout,simout(:,1));
xlabel('Time [s]');
ylabel('Horizontal Displacement [m]');

hf5=figure(5);
set(hf5,'Name','CL: th');
plot(tout,simout(:,2));
xlabel('Time [s]');
ylabel('Pitch Angle [rad]');

hf6=figure(6);
set(hf6,'Name','CL: psi');
plot(tout,simout(:,3));
xlabel('Time [s]');
ylabel('Yaw Angle [rad]');

hf7=figure(7);
set(hf7,'Name','CL: vd1');
plot(tout,simout(:,11));
xlabel('Time [s]');
ylabel('Direct Voltage 1');

hf8=figure(8);
set(hf8,'Name','CL: vq1');
plot(tout,simout(:,12));
xlabel('Time [s]');
ylabel('Quadrature Voltage 1');

hf9=figure(9);
set(hf9,'Name','CL: vd2');
plot(tout,simout(:,13));
xlabel('Time [s]');
ylabel('Direct Voltage 2');

hf10=figure(10);
set(hf10,'Name','CL: vq2');
plot(tout,simout(:,14));
xlabel('Time [s]');
ylabel('Quadrature Voltage 2');

t_int = (min(tout):0.04:max(tout));
glob_int = interp1(tout,glob_pos,t_int);
x_int = interp1(tout,simout(:,1),t_int);
th_int = interp1(tout,simout(:,2),t_int);


%% Animate
Wheel = r*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
           -1                 0];
        
Body = [-.75*r 0
         .75*r 0
         .75*r 1.5*d
        -.75*r 1.5*d];
    
BaseRotated = zeros(size(Wheel,1),size(Wheel,2),length(t_int));
BodyRotated = zeros(size(Body,1),size(Body,2),length(t_int));

for n = 1:length(t_int)
    R1 = [cos(x_int(n)/r) sin(x_int(n)/r)
         -sin(x_int(n)/r) cos(x_int(n)/r)];
    R2 = [cos(th_int(n)) sin(th_int(n))
         -sin(th_int(n)) cos(th_int(n))];
    BaseRotated(:,:,n) = Wheel*R1' + [1 0]*x_int(n);
    BodyRotated(:,:,n) = Body*R2' + [1 0]*x_int(n);
end

hf11=figure(11);
set(hf11,'Name','CL: Animation');
tic;
for n=1:length(glob_int)
    figure(hf11);
    subplot(1,2,1);
        plot(glob_pos(:,1),glob_pos(:,2),glob_int(n,1),glob_int(n,2),'o');
        xlabel('X displacement');
        ylabel('Y displacement');
        title('Top View');
        axis equal;
    subplot(1,2,2);
        plot([min(x_int)-r max(x_int)+r],[-r -r],'linewidth',2);
        hold on;
        fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
        fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
        hold off;
        axis equal;
        xlim(x_int(n)+[-1.2 1.2]);
        ylim((2*d)*[-1 1]);
        xlabel('x displacement');
        ylabel('Z displacement');
        title('Side View');
        grid on;
    drawnow limitrate;
end
toc;