function y = h1(x_km,u)
global P
m1=2*(25e-3+150e-3);
m2=2*(150e-3)+400e-3;
I1=2*(50e-6+24e-6);
I2=1000e-6;
r1=90e-3/2;
r2=75e-3;
g=9.81;
b1=1e-3;
b2=1e-4;
m = m1+m2+I1/(r1^2);
I = m2*r2^2+I2;

% K = P.K;
% N = P.N;
% u= N*r-K*x_km;
% if u>0.5
%     u=0.5;
% elseif u<-0.5
%     u=-0.5;
% end

C = [ 1/r1      1       0       0
      0         1       0       0
      0         0       1/r1    1
      0         0       0       1 ];

D = [0
     0
     0
     0];
 
 y = C*x_km +D*u;