%%
global P

%% Define parameters and initial conditions
P = struct('m1', 2*(25e-3+150e-3),...
           'm2', 2*(150e-3)+400e-3,...
           'I1', 2*(50e-6+24e-6),...
           'I2', 1000e-6,...
           'r1', (90e-3/2),...
           'r2', 75e-3,...
           'g',  9.81,...
           'b1', 1e-3,...
           'b2', 1e-4);
P.m = P.m1+P.m2+P.I1/(P.r1^2);
P.I = P.m2*P.r2^2+P.I2;


% x0 = [0;0;0;pi/12];
x0 = [0;0;0;0];


tstep = 0.075;
tfinal = 30;

reference = (0:tstep/10:tfinal)';
reference(:,2) = 0;
reference(:,3) = 0;
reference(:,4) = -1/P.r1;
% reference(:,4) = 0;
reference(mod(floor(reference(:,1)/10),2)==0,4) = 0;%1/P.r1;
reference(:,5) = 0;


M = [ P.m       -P.m2*P.r2   0   0
     -P.m2*P.r2  P.I         0   0
      0          0           1   0
      0          0           0   1 ];

A = [-(P.b2/P.r1^2+P.b1) -P.b2/P.r1   0   0
     -P.b2/P.r1          -P.b2        0   P.m2*P.r2*P.g
      1                   0           0   0
      0                   1           0   0 ];

B = [ 1/P.r1
      1
      0
      0 ];

A = M\A;
B = M\B;

% C = [0 0 1 0
%      1 0 0 0
%      0 1 0 0
%      0 0 0 1];

C = [ 1/P.r1    1       0       0
      0         1       0       0
      0         0       1/P.r1  1
      0         0       0       1 ];

D = [0
     0
     0
     0];

Q = diag([1/0.005^2    1/0.005^2   1/0.003^2   1/0.010^2]);
R = 1/(0.50^2);
 
Ts = 1/250;
% [K,~,poles] = lqr(A,B,Q,R);
[K,~,poles] = lqrd(A,B,Q,R,Ts);


%[Ad, Bd, Cd, Dd] = ssdata(c2d(ss(A,B,C,D)),Ts);


N = K/(C-D*K);
P.K = K;
P.N = N;
P.A = A;
P.B = B;
P.C = C;
P.D = D;

% Process noise covariance
P.Q = [Ts^3/3  0       Ts^2/2  0
       0       Ts^3/3  0       Ts^2/2
       Ts^2/2  0       Ts      0
       0       Ts^2/2  0       Ts    ]/100;

% Measurement noise covariance
P.R = [0.25  0     0    0
       0    0.003  0    0
       0    0     0.1    0
       0    0     0    0.00025];

load_system('BalanceBotKalmanModel');
mdlWks = get_param('BalanceBotKalmanModel', 'ModelWorkspace');
assignin(mdlWks,'P',P);
%% Run the simulation
sim('BalanceBotKalmanModel',tfinal);

%% Make Plots
figure(1);
plot(tout,r(:,3),tout,y(:,3));
legend('Reference','Output');
xlabel('Time [s]');
ylabel('Motor Angle [rad]');

xmeas = yh/(C.');

figure(2);
subplot(4,1,1);
    plot(tout, x(:,1), tout, xmeas(:,1), t, xhat(:,1));
    xlabel('Time [s]');
    ylabel('Outputs');
    legend('True State','Measured State','Estimated State');
subplot(4,1,2);
    plot(tout, x(:,2), tout, xmeas(:,2), t, xhat(:,2));
    xlabel('Time [s]');
    ylabel('Outputs');
    legend('True State','Measured State','Estimated State');
subplot(4,1,3);
    plot(tout, x(:,3), tout, xmeas(:,3), t, xhat(:,3));
    xlabel('Time [s]');
    ylabel('Outputs');
    legend('True State','Measured State','Estimated State');
subplot(4,1,4);
    plot(tout, x(:,4), tout, xmeas(:,4), t, xhat(:,4));
    xlabel('Time [s]');
    ylabel('Outputs');
    legend('True State','Measured State','Estimated State');

figure(3);
plot(tout,u);
xlabel('Time [s]');
ylabel('Torque [Nm]');

%% Animate
anim = false;

Base = P.r1*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
             -1               0];
        
Body = [-.5*P.r1 0
        .5*P.r1  0
        .5*P.r1  2*P.r2
        -.5*P.r1 2*P.r2];
    
BaseRotated = zeros(size(Base,1),size(Base,2),length(tout));
BodyRotated = zeros(size(Body,1),size(Body,2),length(tout));

tic
for n = 1:length(tout)
    R1 = [cos(-x(n,3)/P.r1) sin(-x(n,3)/P.r1)
         -sin(-x(n,3)/P.r1) cos(-x(n,3)/P.r1)];
    R2 = [cos(x(n,4)) sin(x(n,4))
         -sin(x(n,4)) cos(x(n,4))];
    BaseRotated(:,:,n) = Base*R1 + [1 0]*x(n,3);
    BodyRotated(:,:,n) = Body*R2 + [1 0]*x(n,3);
end
if anim
hf4=figure(4);
for n=1:25:length(tout)
    figure(hf4);
    plot([-5 5],[-P.r1 -P.r1],'linewidth',2);
    hold on;
    plot(P.r1*[r(n,3) r(n,3)], (P.r1+2*P.r2)*[-1 1]);
    fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
    fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
    hold off;
    axis equal;
    %xlim((P.r1+P.r2)*[-5 5]);
    xlim([-1.2 1.2]);
    ylim((P.r1+2*P.r2)*[-1 1]);
    grid on;
    drawnow;
%     frame = getframe(hf1);
%     im{n} = frame2im(frame);
end
toc
end
%%
% filename = 'BalanceBotControlled.gif';
% for n = 1:length(tout)
%     [A,map] = rgb2ind(im{n},256);
%     if n == 1
%         imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',tstep);
%     else
%         imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',tstep);
%     end
% end