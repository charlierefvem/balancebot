% Define symbolic variables for matrix A
syms A_32 A_42

% Define the matrix A
A = [   0   0       1   0
        0   0       0   1
        0   A_32    0   0
        0   A_42    0   0 ];

% Define symbolic variables for matrix C
syms C_11 C_33

% Define the matrix C
C = [   C_11   -1   0       0
        0       1   0       0
        0       0   C_33   -1
        0       0   0       1 ];

% Compute the controllability Gramian
obsv = [C
        C*A
        C*A^2
        C*A^3];

% Check the rank of the Gramian
rank(obsv)