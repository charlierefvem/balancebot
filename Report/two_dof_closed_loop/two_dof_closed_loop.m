%% Balance Bot EOM Demonstration
% *Author:*   Charlie Refvem
%
% *Date:*     05/06/19
%
% *License:*  CC-BY-NC-SA
%
% *Description:*
%
% This script is meant to demonstrate the equations of motion by running a
% Simulink model to simulate the open- and closed-loop responses of the
% balance bot.
%% Fix path
addpath '..\'

%% Define simulation parameters
p = get_params();
K = getGains(p);

%% Initial Conditions
x0 = [  0                               % Initial position [m]
        pi/24                           % Initial angle [rad]
        0                               % Initial velocity [m/s]
        0];                             % Initial angular velocity [rad/s]

%% Simulation Results
simout = sim('two_dof_closed_loop_model');

%% Open Loop Response
fontsize = 11;

hf1=figure(1);
pos=hf1.Position;
pos(4) = 280;
hf1.set('Name','Horizontal Displacement','renderer','painters','position',pos);
plot(simout.t, simout.x(:,1),'k','linewidth',1);
% title('Horizontal Displacement','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$x~[m]$','interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_2DOF_x1.pdf',gcf,600);

hf2=figure(2);
pos=hf2.Position;
pos(4) = 280;
hf2.set('Name','Angular Displacement','position',pos);
plot(simout.t, simout.x(:,2),'k','linewidth',1);
% title('Angular Displacement','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
yticks([-pi/48 0 pi/48 pi/24]);
yticklabels({'$-\frac{\pi}{48}$' '$0$' '$\frac{\pi}{48}$' '$\frac{\pi}{24}$'}); 
ylabel('$\theta~[rad]$','interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_2DOF_x2.pdf',gcf,600);

hf3=figure(3);
pos=hf3.Position;
pos(4) = 280;
hf3.set('Name','Horizontal Velocity','position',pos);
plot(simout.t, simout.x(:,3),'k','linewidth',1);
% title('Horizontal Velocity','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$\dot{x}~[m/s]$','interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_2DOF_x3.pdf',gcf,600);

hf4=figure(4);
pos=hf4.Position;
pos(4) = 280;
hf4.set('Name','Angular Velocity','position',pos);
plot(simout.t, simout.x(:,4),'k','linewidth',1);
% title('Angular Velocity','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$\dot\theta~[rad/s]$','interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_2DOF_x4.pdf',gcf,600);

hf5=figure(5);
pos=hf5.Position;
pos(4) = 280;
hf5.set('Name','Angular Velocity','position',pos);
plot(simout.t, simout.u,'k','linewidth',1);
% title('Motor Torque','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$T~[Nm]$','interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_2DOF_u.pdf',gcf,600);

%% Animation
Wheel = p.rw*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
              -1                 0];
        
Body = [-.75*p.rw 0
         .75*p.rw 0
         .75*p.rw 5*p.rb
        -.75*p.rw 5*p.rb];

fps=20;
t_int = (0:1/fps:max(simout.t))';
x_int = interp1(simout.t,simout.x(:,1),t_int);
th_int = interp1(simout.t,simout.x(:,2),t_int);

BaseRotated = zeros(size(Wheel,1),size(Wheel,2),length(t_int));
BodyRotated = zeros(size(Body,1),size(Body,2),length(t_int));

for n = 1:length(t_int)
    R1 = [cos(x_int(n)/p.rw) sin(x_int(n)/p.rw)
         -sin(x_int(n)/p.rw) cos(x_int(n)/p.rw)];
    R2 = [cos(th_int(n)) sin(th_int(n))
         -sin(th_int(n)) cos(th_int(n))];
    BaseRotated(:,:,n) = Wheel*R1' + [1 0]*x_int(n);
    BodyRotated(:,:,n) = Body*R2' + [1 0]*x_int(n);
end
hf6=figure(6);
pos=hf6.Position;
pos(4) = 280;
set(hf6,'Name','Animation','renderer','painters','position',pos);
tic;
im = cell(length(t_int),1);
for n=1:length(t_int)
    figure(hf6);
    plot([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw],[-p.rw -p.rw],'linewidth',2);
    hold on;
    fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
    fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
    hold off;
    axis equal;
    xlim([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw]);
    ylim([-0.2 0.2]);
    xlabel('Horizontal displacement','interpreter','latex','fontsize',fontsize);
    ylabel({'Vertical';'displacement'},'interpreter','latex','fontsize',fontsize);
%     title('Side View');
    grid on;
    set(gca,'TickLabelInterpreter','latex');
    drawnow;
    im{n} = frame2im(getframe(hf6));
end
toc;

filename = 'CL_2DOF_anim.gif'; % Specify the output file name
for idx = 1:length(im)
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',inf,'DelayTime',1/fps);
    elseif idx == length(im)
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',2);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',1/fps);
    end
end
fprintf('File "%s" created successfully!\n',filename);

save2pdf('CL_2DOF_anim.pdf',gcf,600);