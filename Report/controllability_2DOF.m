% Define symbolic variables for matrix A
syms A_32 A_42

% Define symbolic variables for matrix B
syms B_3 B_4

% Define the matrix A
A = [   0   0       1   0
        0   0       0   1
        0   A_32    0   0
        0   A_42    0   0 ]

% Define the matrix B
B = [   0
        0
        B_3
        B_4 ]

% Compute the controllability Gramian
ctrl = [B A*B A^2*B A^3*B]

% Check the rank of the Gramian
rank(ctrl)