% Define symbolic variables for matrix A
syms A_42 A_52

% Define the matrix A
A = [   0   0      0   1   0   0
        0   0      0   0   1   0
        0   0      0   0   0   1
        0   A_42   0   0   0   0
        0   A_52   0   0   0   0
        0   0      0   0   0   0 ];

% Define symbolic variables for matrix B
syms B_41 B_42 B_51 B_52 B_61 B_62

% Define the matrix B
B = [   0       0
        0       0
        0       0
        B_41    B_42
        B_51    B_52
        B_61    B_62 ];

% Compute the controllability Gramian
ctrl = [B A*B A^2*B A^3*B A^4*B A^5*B];

% Check the rank of the Gramian
rank(ctrl)