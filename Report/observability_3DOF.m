% Define symbolic variables for matrix A
syms A_42 A_52 B_41

% Define the matrix A
A = [   0   0      0   1   0   0
        0   0      0   0   1   0
        0   0      0   0   0   1
        0   A_42   0   0   0   0
        0   A_52   0   0   0   0
        0   0      0   0   0   0 ];

% Define symbolic variables for matrix C
syms C_11 C_44

% Define the matrix C
C = [   C_11   -1   0   0       0   0
        0       1   0   0       0   0
        0       0   1   0       0   0
        0       0   0   C_44   -1   0
        0       0   0   0       1   0
        0       0   0   0       0   1];

% Compute the controllability Gramian
obsv = [C
        C*A
        C*A^2
        C*A^3
        C*A^4
        C*A^5];

% Check the rank of the Gramian
rank(obsv)