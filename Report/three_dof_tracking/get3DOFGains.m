function [K,Q,R] = getGains(p)
% M = [   1   0   0   0           0           0
%         0   1   0   0           0           0
%         0   0   1   0           0           0
%         0   0   0   p.mx        p.mb*p.rb*c 0
%         0   0   0   p.mb*p.rb*c p.Ith       0
%         0   0   0   0           0           p.mb*p.rb^2*s^2+p.Ibxx*s^2+p.Ibzz*c^2+Ips];

A = zeros(6);
A(1,4) = 1;
A(2,5) = 1;
A(3,6) = 1;
A(4,2) =-(p.mb^2*p.rb^2*p.g)/(p.mx*p.Ith - p.mb^2*p.rb^2);
A(5,2) = (p.mx*p.mb*p.rb*p.g)/(p.mx*p.Ith - p.mb^2*p.rb^2);

B = zeros(6,2);
B(4,1) =-(p.Ith/p.rw + p.mb*p.rb)/(p.mx*p.Ith - p.mb^2*p.rb^2);
B(4,2) = (p.Ith/p.rw + p.mb*p.rb)/(p.mx*p.Ith - p.mb^2*p.rb^2);
B(5,1) = (p.mb*p.rb/p.rw + p.mx)/(p.mx*p.Ith - p.mb^2*p.rb^2);
B(5,2) =-(p.mb*p.rb/p.rw + p.mx)/(p.mx*p.Ith - p.mb^2*p.rb^2);
B(6,1) =-(p.w/(2*p.rw))/(p.Ibzz+p.Ips);
B(6,2) =-(p.w/(2*p.rw))/(p.Ibzz+p.Ips);

x1max = 0.01;
x3max = 1;
x2max = 1;
x4max = 0.005;
x5max = 0.01;
x6max = 0.01;
u1max  = 0.25;


Q = diag(1./[x1max x2max x3max x4max x5max x6max].^2);
R = diag(1./[u1max u1max].^2);
    

Ts = 1/40;


K = lqrd(A,B,Q,R,Ts);

end

