%% Fix path
addpath '..\';

%% Define simulation parameters
p = get_params();
[K3DOF, Q, R] = get3DOFGains(p);

%% Define prefilter parameters
bx   = 2;
bpsi = 2;
athr = 1;
astr = 1;

sig = (p.mx*p.rw+p.mb*p.rb)/(p.mb*p.rb*p.g);

A = [   0               0           1           0
        0               0           0           1
        0               0          -bx          0
        0               0           0          -bpsi ];

B = [   0               0
        0               0
        bx*athr         0
        0               bpsi*astr ];

C = [   1               0           0           0
        0               0          -sig*bx      0
        0               1           0           0
        0               0           1           0
        0               0           sig*bx^2    0
        0               0           0           1 ];

D = [   0               0
        sig*bx*athr     0
        0               0
        0               0
       -sig*bx^2*athr   0
        0               0 ];
%%
simout = sim('prefilter_only');

%% Plots
fontsize = 11;

hf1=figure(1);
pos=hf1.Position;
pos(4) = 560;
hf1.set('Name','Horizontal Displacement','renderer','painters','position',pos);
subplot(2,1,1)
    plot(simout.t, simout.r(:,4),'k','linewidth',1,'DisplayName','Velocity Reference');
    hold on;
    plot(simout.t, simout.cmd(:,1),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Throttle Command');
    hold off;
    grid on;
    xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
    ylabel('Horizontal Velocity $[m/s]$','interpreter','latex','fontsize',fontsize);
    legend('Interpreter','latex','fontsize',fontsize,'location','northeast');
    set(gca,'TickLabelInterpreter','latex');
    save2pdf('TR_3DOF_x1.pdf',gcf,600);
subplot(2,1,2)
    plot(simout.t, simout.r(:,2),'k','linewidth',1,'DisplayName','Pitch Reference');
%     hold on;
%     plot(simout.t, simout.cmd(:,2),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Throttle Command');
%     hold off;
    grid on;
    xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
    ylabel('Pitch Angle $[rad]$','interpreter','latex','fontsize',fontsize);
%     legend('Interpreter','latex','fontsize',fontsize,'location','southeast');
    set(gca,'TickLabelInterpreter','latex');
    save2pdf('TR_3DOF_x1.pdf',gcf,600);

% hf2=figure(2);
% pos=hf2.Position;
% pos(4) = 280;
% hf2.set('Name','Pitch Angle','position',pos);
% plot(simout.t, simout.r(:,2),'k','linewidth',1,'DisplayName','Reference');
% hold on;
% plot(simout.t, simout.x(:,2),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
% hold off;
% % title('Angular Displacement','interpreter','latex');
% grid on;
% xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
% % yticks([-pi/48 0 pi/48 pi/24]);
% % yticklabels({'$-\frac{\pi}{48}$' '$0$' '$\frac{\pi}{48}$' '$\frac{\pi}{24}$'}); 
% ylabel('$\theta~[rad]$','interpreter','latex','fontsize',fontsize);
% legend('Interpreter','latex','fontsize',fontsize);
% set(gca,'TickLabelInterpreter','latex');
% save2pdf('TR_3DOF_x2.pdf',gcf,600);
% 
% hf3=figure(3);
% pos=hf3.Position;
% pos(4) = 280;
% hf3.set('Name','Yaw Angle','position',pos);
% plot(simout.t, simout.r(:,3),'k','linewidth',1,'DisplayName','Reference');
% hold on;
% plot(simout.t, simout.x(:,3),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
% hold off;
% % title('Angular Displacement','interpreter','latex');
% grid on;
% xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
% yticks([0 pi/2 pi 3*pi/2 2*pi]);
% yticklabels({'$0$' '$\frac{\pi}{2}$' '$\pi$' '$\frac{3\pi}{2}$' '$2\pi$'}); 
% ylabel('$\psi~[rad]$','interpreter','latex','fontsize',fontsize);
% legend('Interpreter','latex','fontsize',fontsize,'location','southeast');
% set(gca,'TickLabelInterpreter','latex');
% save2pdf('TR_3DOF_x3.pdf',gcf,600);
% 
% hf4=figure(4);
% pos=hf4.Position;
% pos(4) = 280;
% hf4.set('Name','Horizontal Velocity','position',pos);
% plot(simout.t, simout.r(:,4),'k','linewidth',1,'DisplayName','Reference');
% hold on;
% plot(simout.t, simout.x(:,4),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
% hold off;
% % title('Horizontal Velocity','interpreter','latex');
% grid on;
% xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
% ylabel('$\dot{x}~[m/s]$','interpreter','latex','fontsize',fontsize);
% legend('Interpreter','latex','fontsize',fontsize);
% set(gca,'TickLabelInterpreter','latex');
% save2pdf('TR_3DOF_x4.pdf',gcf,600);
% 
% hf6=figure(5);
% pos=hf6.Position;
% pos(4) = 280;
% hf6.set('Name','Pitch Velocity','position',pos);
% plot(simout.t, simout.r(:,5),'k','linewidth',1,'DisplayName','Reference');
% hold on;
% plot(simout.t, simout.x(:,5),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
% hold off;
% % title('Angular Velocity','interpreter','latex');
% grid on;
% xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
% ylabel('$\dot\theta~[rad/s]$','interpreter','latex','fontsize',fontsize);
% legend('Interpreter','latex','fontsize',fontsize);
% set(gca,'TickLabelInterpreter','latex');
% save2pdf('TR_3DOF_x5.pdf',gcf,600);
% 
% hf6=figure(6);
% pos=hf6.Position;
% pos(4) = 280;
% hf6.set('Name','Yaw Velocity','position',pos);
% plot(simout.t, simout.r(:,6),'k','linewidth',1,'DisplayName','Reference');
% hold on;
% plot(simout.t, simout.x(:,6),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
% hold off;
% % title('Angular Velocity','interpreter','latex');
% grid on;
% xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
% ylabel('$\dot\psi~[rad/s]$','interpreter','latex','fontsize',fontsize);
% legend('Interpreter','latex','fontsize',fontsize);
% set(gca,'TickLabelInterpreter','latex');
% save2pdf('TR_2DOF_x6.pdf',gcf,600);