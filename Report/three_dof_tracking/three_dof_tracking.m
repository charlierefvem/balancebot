%% Balance Bot EOM Demonstration
% *Author:*   Charlie Refvem
%
% *Date:*     05/06/19
%
% *License:*  CC-BY-NC-SA
%
% *Description:*
%
% This script is meant to demonstrate the equations of motion by running a
% Simulink model to simulate the open- and closed-loop responses of the
% balance bot.
%% Fix path
addpath '..\';

%% Define simulation parameters
p = get_params();
[K3DOF, Q, R] = get3DOFGains(p);

%% Define prefilter parameters
bx   = 2;
bpsi = 2;
athr = 1;
astr = 1;

sig = (p.mx*p.rw+p.mb*p.rb)/(p.mb*p.rb*p.g);

A = [   0               0           1           0
        0               0           0           1
        0               0          -bx          0
        0               0           0          -bpsi ];

B = [   0               0
        0               0
        bx*athr         0
        0               bpsi*astr ];

C = [   1               0           0           0
        0               0          -sig*bx      0
        0               1           0           0
        0               0           1           0
        0               0           sig*bx^2    0
        0               0           0           1 ];

D = [   0               0
        sig*bx*athr     0
        0               0
        0               0
       -sig*bx^2*athr   0
        0               0 ];

%% Initial Conditions
x03DOF = [  0                               % Initial position [m]
            0                               % Initial pitch angle [rad]
            0                               % Initial yaw angle [rad]
            0                               % Initial velocity [m/s]
            0                               % Initial pitch velocity [rad/s]
            0];                             % Initial yaw velocity [rad/s]

%% Simulation Results
simout = sim('three_dof_tracking_model');
J = 0;
for n=1:length(simout.t)
    J = J + simout.x(n,:)*Q*simout.x(n,:)' + simout.u(n,:)*R*simout.u(n,:)';
end
fprintf('Cost Index J=%2.2e\n',J);

%% Tracking Response
fontsize = 11;

hf1=figure(1);
pos=hf1.Position;
pos(4) = 280;
hf1.set('Name','Horizontal Displacement','renderer','painters','position',pos);
plot(simout.t, simout.r(:,1),'k','linewidth',1,'DisplayName','Reference');
hold on;
plot(simout.t, simout.x(:,1),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
hold off;
% title('Horizontal Displacement','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$x~[m]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize,'location','southeast');
set(gca,'TickLabelInterpreter','latex');
save2pdf('TR_3DOF_x1.pdf',gcf,600);

hf2=figure(2);
pos=hf2.Position;
pos(4) = 280;
hf2.set('Name','Pitch Angle','position',pos);
plot(simout.t, simout.r(:,2),'k','linewidth',1,'DisplayName','Reference');
hold on;
plot(simout.t, simout.x(:,2),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
hold off;
% title('Angular Displacement','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
% yticks([-pi/48 0 pi/48 pi/24]);
% yticklabels({'$-\frac{\pi}{48}$' '$0$' '$\frac{\pi}{48}$' '$\frac{\pi}{24}$'}); 
ylabel('$\theta~[rad]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('TR_3DOF_x2.pdf',gcf,600);

hf3=figure(3);
pos=hf3.Position;
pos(4) = 280;
hf3.set('Name','Yaw Angle','position',pos);
plot(simout.t, simout.r(:,3),'k','linewidth',1,'DisplayName','Reference');
hold on;
plot(simout.t, simout.x(:,3),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
hold off;
% title('Angular Displacement','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
yticks([0 pi/2 pi 3*pi/2 2*pi]);
yticklabels({'$0$' '$\frac{\pi}{2}$' '$\pi$' '$\frac{3\pi}{2}$' '$2\pi$'}); 
ylabel('$\psi~[rad]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize,'location','southeast');
set(gca,'TickLabelInterpreter','latex');
save2pdf('TR_3DOF_x3.pdf',gcf,600);

hf4=figure(4);
pos=hf4.Position;
pos(4) = 280;
hf4.set('Name','Horizontal Velocity','position',pos);
plot(simout.t, simout.r(:,4),'k','linewidth',1,'DisplayName','Reference');
hold on;
plot(simout.t, simout.x(:,4),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
hold off;
% title('Horizontal Velocity','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$\dot{x}~[m/s]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('TR_3DOF_x4.pdf',gcf,600);

hf6=figure(5);
pos=hf6.Position;
pos(4) = 280;
hf6.set('Name','Pitch Velocity','position',pos);
plot(simout.t, simout.r(:,5),'k','linewidth',1,'DisplayName','Reference');
hold on;
plot(simout.t, simout.x(:,5),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
hold off;
% title('Angular Velocity','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$\dot\theta~[rad/s]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('TR_3DOF_x5.pdf',gcf,600);

hf6=figure(6);
pos=hf6.Position;
pos(4) = 280;
hf6.set('Name','Yaw Velocity','position',pos);
plot(simout.t, simout.r(:,6),'k','linewidth',1,'DisplayName','Reference');
hold on;
plot(simout.t, simout.x(:,6),'--','color',[0.75 0.75 0.75],'linewidth',1,'DisplayName','Response');
hold off;
% title('Angular Velocity','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$\dot\psi~[rad/s]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('TR_2DOF_x6.pdf',gcf,600);

hf7=figure(7);
pos=hf7.Position;
pos(4) = 280;
hf7.set('Name','Input Torque','position',pos);
plot(simout.t, simout.u(:,1),'k','linewidth',1,'DisplayName','$T_{m,r}$');
hold on;
plot(simout.t, simout.u(:,2),'--k','linewidth',1,'DisplayName','$T_{m,l}$');
hold off;
% title('Motor Torque','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$T~[Nm]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('TR_3DOF_u.pdf',gcf,600);

hf8=figure(8);
pos=hf8.Position;
pos(4) = 280;
hf8.set('Name','Throttle Command','position',pos);
plot(simout.t, simout.cmd(:,1),'k','linewidth',1);
% title('Motor Torque','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('Throttle Command','interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('TR_3DOF_thr.pdf',gcf,600);

hf9=figure(9);
pos=hf9.Position;
pos(4) = 280;
hf9.set('Name','Throttle Command','position',pos);
plot(simout.t, simout.cmd(:,2),'k','linewidth',1);
% title('Motor Torque','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
yticks([0 pi/4 pi/2 3*pi/4 pi]);
yticklabels({'$0$' '$\frac{\pi}{4}$' '$\frac{\pi}{2}$' '$\frac{3\pi}{4}$' '$\pi$'}); 
ylabel('Steering Command','interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('TR_3DOF_str.pdf',gcf,600);

%% Animation
Wheel = p.rw*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
              -1                 0];
        
Body = [-.75*p.rw 0
         .75*p.rw 0
         .75*p.rw 5*p.rb
        -.75*p.rw 5*p.rb];

fps = 30;
t_int = (0:1/fps:max(simout.t))';
x_int = interp1(simout.t,simout.x(:,1),t_int);
th_int = interp1(simout.t,simout.x(:,2),t_int);

BaseRotated = zeros(size(Wheel,1),size(Wheel,2),length(t_int));
BodyRotated = zeros(size(Body,1),size(Body,2),length(t_int));

for n = 1:length(t_int)
    R1 = [cos(x_int(n)/p.rw) sin(x_int(n)/p.rw)
         -sin(x_int(n)/p.rw) cos(x_int(n)/p.rw)];
    R2 = [cos(th_int(n)) sin(th_int(n))
         -sin(th_int(n)) cos(th_int(n))];
    BaseRotated(:,:,n) = Wheel*R1' + [1 0]*x_int(n);
    BodyRotated(:,:,n) = Body*R2' + [1 0]*x_int(n);
end
hf10=figure(10);
pos=hf10.Position;
pos(4) = 280;
set(hf10,'Name','Animation','renderer','painters','position',pos);
tic;
im = cell(length(t_int),1);
for n=1:length(t_int)
    figure(hf10);
    plot([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw],[-p.rw -p.rw],'linewidth',2);
    hold on;
    fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
    fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
    hold off;
    axis equal;
    xlim([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw]);
    ylim([-0.2 0.2]);
    xlabel('Horizontal displacement','interpreter','latex','fontsize',fontsize);
    ylabel({'Vertical';'displacement'},'interpreter','latex','fontsize',fontsize);
%     title('Side View');
    grid on;
    set(gca,'TickLabelInterpreter','latex');
    drawnow;
    im{n} = frame2im(getframe(hf10));
end
toc;

filename = 'TR_3DOF_anim.gif'; % Specify the output file name
for idx = 1:length(im)
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',inf,'DelayTime',1/fps);
    elseif idx == length(im)
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',2);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',1/fps);
    end
end
fprintf('File "%s" created successfully!\n',filename);
save2pdf('TR_3DOF_anim.pdf',gcf,600);