function [K,A,B] = getGains(p)
M = [   1   0   0           0
        0   1   0           0
        0   0   p.mx        p.mb*p.rb
        0   0   p.mb*p.rb   p.Ith ];

A = [   0   0           1   0
        0   0           0   1
        0   0           0   0
        0   p.mb*p.rb*p.g 0   0 ];

B = [   0
        0
       -2/p.rw
        2 ];

x1max = 0.01;
x2max = 1;
x3max = 0.005;
x4max = 0.01;
umax  = 0.5;


Q = diag(1./[x1max x2max x3max x4max].^2);
R = diag(1./[umax].^2);
    

Ts = 1/40;

A = M\A;
B = M\B;

K = lqrd(A,B,Q,R,Ts);

end

