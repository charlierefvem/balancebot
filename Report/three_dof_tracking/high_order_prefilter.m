%% Fix path
addpath '..\';

%% Define simulation parameters
p = get_params();
K = getGains(p);

simout=sim('high_order_prefilter_model');

%% Animation
Wheel = p.rw*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
              -1                 0];
        
Body = [-.75*p.rw 0
         .75*p.rw 0
         .75*p.rw 5*p.rb
        -.75*p.rw 5*p.rb];

fps=20;
t_int = (0:1/fps:max(simout.tout))';
x_int = interp1(simout.tout,simout.x(:,1),t_int);
th_int = interp1(simout.tout,simout.x(:,2),t_int);

BaseRotated = zeros(size(Wheel,1),size(Wheel,2),length(t_int));
BodyRotated = zeros(size(Body,1),size(Body,2),length(t_int));

fontsize = 11;
for n = 1:length(t_int)
    R1 = [cos(x_int(n)/p.rw) sin(x_int(n)/p.rw)
         -sin(x_int(n)/p.rw) cos(x_int(n)/p.rw)];
    R2 = [cos(th_int(n)) sin(th_int(n))
         -sin(th_int(n)) cos(th_int(n))];
    BaseRotated(:,:,n) = Wheel*R1' + [1 0]*x_int(n);
    BodyRotated(:,:,n) = Body*R2' + [1 0]*x_int(n);
end
hf6=figure(6);
pos=hf6.Position;
pos(4) = 280;
set(hf6,'Name','Animation','renderer','painters','position',pos);
tic;
im = cell(length(t_int),1);
for n=1:length(t_int)
    figure(hf6);
    plot([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw],[-p.rw -p.rw],'linewidth',2);
    hold on;
    fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
    fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
    hold off;
    axis equal;
    xlim([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw]);
    ylim([-0.2 0.2]);
    xlabel('Horizontal displacement','interpreter','latex','fontsize',fontsize);
    ylabel({'Vertical';'displacement'},'interpreter','latex','fontsize',fontsize);
%     title('Side View');
    grid on;
    set(gca,'TickLabelInterpreter','latex');
    drawnow;
    im{n} = frame2im(getframe(hf6));
end
toc;

filename = 'CL_2DOF_anim.gif'; % Specify the output file name
for idx = 1:length(im)
    [A,map] = rgb2ind(im{idx},256);
    if idx == 1
        imwrite(A,map,filename,'gif','LoopCount',inf,'DelayTime',1/fps);
    elseif idx == length(im)
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',2);
    else
        imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',1/fps);
    end
end
fprintf('File "%s" created successfully!\n',filename);

save2pdf('CL_2DOF_anim.pdf',gcf,600);