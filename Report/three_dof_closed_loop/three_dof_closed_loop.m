%% Balance Bot EOM Demonstration
% *Author:*   Charlie Refvem
%
% *Date:*     05/06/19
%
% *License:*  CC-BY-NC-SA
%
% *Description:*
%
% This script is meant to demonstrate the equations of motion by running a
% Simulink model to simulate the open- and closed-loop responses of the
% balance bot.
%% Fix path
addpath '..\'

%% Define simulation parameters
p = get_params();
K2DOF = get2DOFGains(p);
K3DOF = get3DOFGains(p);

%% Initial Conditions
x02DOF = [  0                               % Initial position [m]
            pi/24                           % Initial pitch angle [rad]
            0                               % Initial velocity [m/s]
            0];                             % Initial pitch velocity [rad/s]
        
x03DOF = [  0                               % Initial position [m]
            pi/24                           % Initial pitch angle [rad]
            0                               % Initial yaw angle [rad]
            0                               % Initial velocity [m/s]
            0                               % Initial pitch velocity [rad/s]
            0];                             % Initial yaw velocity [rad/s]

%% Simulation Results
simout2DOF = sim('two_dof_closed_loop_model');
simout3DOF = sim('three_dof_closed_loop_model');

%% Open Loop Response
fontsize = 11;

hf1=figure(1);
pos=hf1.Position;
pos(4) = 280;
hf1.set('Name','Horizontal Displacement','renderer','painters','position',pos);
plot(simout3DOF.t, simout3DOF.x(:,1),'k','linewidth',1,'DisplayName', '3-DOF');
hold on;
plot(simout2DOF.t, simout2DOF.x(:,1),'--','linewidth',1,'color',[0.75 0.75 0.75],'DisplayName', '2-DOF');
hold off;
% title('Horizontal Displacement','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$x~[m]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_3DOF_x1.pdf',gcf,600);

hf2=figure(2);
pos=hf2.Position;
pos(4) = 280;
hf2.set('Name','Angular Displacement','position',pos);
plot(simout3DOF.t, simout3DOF.x(:,2),'k','linewidth',1,'DisplayName', '3-DOF');
hold on;
plot(simout2DOF.t, simout2DOF.x(:,2),'--','linewidth',1,'color',[0.75 0.75 0.75],'DisplayName', '2-DOF');
hold off;
% title('Angular Displacement','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
yticks([-pi/48 0 pi/48 pi/24]);
yticklabels({'$-\frac{\pi}{48}$' '$0$' '$\frac{\pi}{48}$' '$\frac{\pi}{24}$'}); 
ylabel('$\theta~[rad]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_3DOF_x2.pdf',gcf,600);

hf3=figure(3);
pos=hf3.Position;
pos(4) = 280;
hf3.set('Name','Angular Displacement','position',pos);
plot(simout3DOF.t, simout3DOF.x(:,3),'k','linewidth',1);
% title('Angular Displacement','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
% yticks([-pi/48 0 pi/48 pi/24]);
% yticklabels({'$-\frac{\pi}{48}$' '$0$' '$\frac{\pi}{48}$' '$\frac{\pi}{24}$'}); 
ylabel('$\psi~[rad]$','interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_3DOF_x3.pdf',gcf,600);

hf4=figure(4);
pos=hf4.Position;
pos(4) = 280;
hf4.set('Name','Horizontal Velocity','position',pos);
plot(simout3DOF.t, simout3DOF.x(:,4),'k','linewidth',1,'DisplayName', '3-DOF');
hold on;
plot(simout2DOF.t, simout2DOF.x(:,3),'--','linewidth',1,'color',[0.75 0.75 0.75],'DisplayName', '2-DOF');
hold off;
% title('Horizontal Velocity','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$\dot{x}~[m/s]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_3DOF_x4.pdf',gcf,600);

hf6=figure(5);
pos=hf6.Position;
pos(4) = 280;
hf6.set('Name','Angular Velocity','position',pos);
plot(simout3DOF.t, simout3DOF.x(:,5),'k','linewidth',1,'DisplayName', '3-DOF');
hold on;
plot(simout2DOF.t, simout2DOF.x(:,4),'--','linewidth',1,'color',[0.75 0.75 0.75],'DisplayName', '2-DOF');
hold off;
% title('Angular Velocity','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$\dot\theta~[rad/s]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_3DOF_x5.pdf',gcf,600);

hf6=figure(6);
pos=hf6.Position;
pos(4) = 280;
hf6.set('Name','Angular Velocity','position',pos);
plot(simout3DOF.t, simout3DOF.x(:,6),'k','linewidth',1);
% title('Angular Velocity','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$\dot\psi~[rad/s]$','interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_3DOF_x6.pdf',gcf,600);

hf7=figure(7);
pos=hf7.Position;
pos(4) = 280;
hf7.set('Name','Angular Velocity','position',pos);
plot(simout3DOF.t, simout3DOF.u(:,1),'k','linewidth',1,'DisplayName','$T_{m,r}$ (3-DOF)');
hold on;
plot(simout3DOF.t, simout3DOF.u(:,2),':k','linewidth',1,'DisplayName','$T_{m,l}$ (3-DOF)');
plot(simout2DOF.t, simout2DOF.u,'--','linewidth',1,'color',[0.75 0.75 0.75],'DisplayName','$T_m$ (2-DOF)');
hold off;
% title('Motor Torque','interpreter','latex');
grid on;
xlabel('$t~[s]$','interpreter','latex','fontsize',fontsize);
ylabel('$T~[Nm]$','interpreter','latex','fontsize',fontsize);
legend('Interpreter','latex','fontsize',fontsize);
set(gca,'TickLabelInterpreter','latex');
save2pdf('CL_2DOF_u.pdf',gcf,600);

%% Animation
Wheel = p.rw*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
              -1                 0];
        
Body = [-.75*p.rw 0
         .75*p.rw 0
         .75*p.rw 5*p.rb
        -.75*p.rw 5*p.rb];


t_int = (0:1/20:max(simout3DOF.t))';
x_int = interp1(simout3DOF.t,simout3DOF.x(:,1),t_int);
th_int = interp1(simout3DOF.t,simout3DOF.x(:,2),t_int);

BaseRotated = zeros(size(Wheel,1),size(Wheel,2),length(t_int));
BodyRotated = zeros(size(Body,1),size(Body,2),length(t_int));

for n = 1:length(t_int)
    R1 = [cos(x_int(n)/p.rw) sin(x_int(n)/p.rw)
         -sin(x_int(n)/p.rw) cos(x_int(n)/p.rw)];
    R2 = [cos(th_int(n)) sin(th_int(n))
         -sin(th_int(n)) cos(th_int(n))];
    BaseRotated(:,:,n) = Wheel*R1' + [1 0]*x_int(n);
    BodyRotated(:,:,n) = Body*R2' + [1 0]*x_int(n);
end
hf9=figure(9);
pos=hf9.Position;
pos(4) = 280;
set(hf9,'Name','Animation','renderer','painters','position',pos);
tic;
for n=1:length(t_int)
    figure(hf9);
    plot([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw],[-p.rw -p.rw],'linewidth',2);
    hold on;
    fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
    fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
    hold off;
    axis equal;
    xlim([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw]);
    ylim([-0.2 0.2]);
    xlabel('Horizontal displacement','interpreter','latex','fontsize',fontsize);
    ylabel({'Vertical';'displacement'},'interpreter','latex','fontsize',fontsize);
%     title('Side View');
    grid on;
    set(gca,'TickLabelInterpreter','latex');
    drawnow;
end
toc;
saveas(gcf,'CL_3DOF_anim.pdf');
save2pdf('CL_3DOF_anim.pdf',gcf,600);