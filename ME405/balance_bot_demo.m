%% Balance Bot EOM Demonstration
% *Author:*   Charlie Refvem
%
% *Date:*     05/06/19
%
% *License:*  CC-BY-NC-SA
%
% *Description:*
%
% This script is meant to demonstrate the equations of motion by running a
% Simulink model to simulate the open- and closed-loop responses of the
% balance bot.

%% System Parameters
p.mw  = 80e-3;                          % [kg]
p.mb  = 800e-3;                         % [kg]
p.rw  = 45e-3;                          % [m]
p.rb  = 25e-3;                          % [m]
p.Iw  = 2.5e-6;                         % [kg*m^2]
p.Ib  = 5.8e-3;                         % [kg*m^2]
p.g   = 9.81;                           % [m/s/s]
p.mx  = p.mb+2*p.mw+2*p.Iw/(p.rw^2);    % [kg]
p.Ith = p.mb*p.rb^2 + p.Ib;             % [kg*m^2]

%% Initial Conditions
x0 = [  0                               % Initial position [m]
        0                           % Initial angle [rad]
        0                               % Initial velocity [m/s]
        0];                             % Initial angular velocity [rad/s]

%% Gain Calculation
%zeta = 1.2;
zeta = 0.6;
omega = 100*2*pi;
pol = [ -zeta*omega-sqrt((zeta^2-1)*omega^2)
        -zeta*omega+sqrt((zeta^2-1)*omega^2)];
pol(3) = 3*real(pol(1));
pol(4) = 4*real(pol(1));M = [   1   0   0           0
        0   1   0           0
        0   0   p.mx        p.mb*p.rb
        0   0   p.mb*p.rb   p.Ith ];

A = [   0   0           1   0
        0   0           0   1
        0   0           0   0
        0   p.mb*p.rb   0   0 ];

B = [   0
        0
        -2/p.rw
        2 ];

x1max = 0.08;
x2max = 0.12;
x3max = 0.12;
x4max = 0.12;
umax  = 0.8;


Q = diag(1./[x1max x2max x3max x4max].^2);
R = diag(1./[umax].^2);
    

Ts = 10e-3;

A = M\A;
B = M\B;
figure(7)
pzmap(ss(A,B,eye(4),0))
title('Open Loop Poles')

T = [1/p.rw   -1   0         0
     0         1   0         0
     0         0   1/p.rw   -1
     0         0   0         1];

%K = place(A,B,poles);
%K = lqr(A,B,Q,R);
% K = lqrd(A,B,Q,R,Ts);

K = [0.0030 0.1916 0.0012 0.0150]*T/1.5

figure(8)
pzmap(ss(A-B*K,zeros(size(B)),eye(4),0))
title('Closed Loop Poles')


Kstar = K/T

%% Discretization
Ts = 1/80;
sys_c = ss(A,B,eye(4),0);
sys_d = c2d(sys_c,Ts);
[Ad, Bd, Cd, Dd] = ssdata(sys_d);

des_sys_c = tf([1],[1 2*zeta*omega omega^2]);
des_sys_d = c2d(des_sys_c,Ts);
[num, den] = tfdata(des_sys_d);
lam_d = roots(den{1});

pd = [lam_d; -sqrt(2)/2+sqrt(2)/2*1i ;-sqrt(2)/2-sqrt(2)/2*1i];
Kd = place(Ad,Bd,pd);

%% Prefilter matrices
kpf = 10;
A42 = p.mb*p.rb*p.g/(p.mb*p.rb*p.rw+p.Ith);
A43 = kpf*(p.mx*p.rw+p.mb*p.rb)/(p.mb*p.rb*p.rw+p.Ith);
b4 = -A43;

% Apf = [ 0       0       1       0
%         0       0       0       1
%         0       0       -kpf    0
%         0       A42     A43     0 ];
% Bpf = [ 0
%         0
%         kpf
%         b4 ];
% Cpf = eye(4);
% Dpf = zeros(4,1);

% x = [x xd]
% y = [x th xd thd]
Apf = [ 0       1
        0       -kpf ];
Bpf = [ 0
        kpf ];
Cpf = [ 1       0
        0       -kpf*(p.mx*p.rw+p.mb*p.rb)/(p.mb*p.rb*p.g)
        0       1
        0       kpf^2*(p.mx*p.rw+p.mb*p.rb)/(p.mb*p.rb*p.g) ];
Dpf = [ 0
        kpf*(p.mx*p.rw+p.mb*p.rb)/(p.mb*p.rb*p.g)
        0
        -kpf^2*(p.mx*p.rw+p.mb*p.rb)/(p.mb*p.rb*p.g) ];

Tstate = [ 1/p.rw   0
           0        1/p.rw ];
Tinput = 1/p.rw;

Apf_ = Tstate*Apf/Tstate;
Bpf_ = Tstate*Bpf/Tinput;
Cpf_ = C/Tstate;
Dpf_ = D/Tinput;

sys_con = ss(Apf_, Bpf_, Cpf_, Dpf_);
sys_dis = c2d(sys_con,Ts);
[Apf_d, Bpf_d, Cpf_d, Dpf_d] = ssdata(sys_dis)

% Cpf = [ 1       0
%         0       -kpf*(p.mx*p.rw+p.mb*p.rb)/(p.mb*p.rb*p.g)
%         0       1
%         0       0 ];
% Dpf = [ 0
%         kpf*(p.mx*p.rw+p.mb*p.rb)/(p.mb*p.rb*p.g)
%         0
%         0 ];
% Cpf = [ 1       0
%         0       0
%         0       1
%         0       0 ];
% Dpf = [ 0
%         0
%         0
%         0 ];

%% Simulation Results
CL = false;
% simout = sim('Model2DOF');
simout = sim('Model2DOF_Tracking');

%% Open Loop Response
hf1=figure(1);
hf1.set('Name','Horizontal Displacement');
plot(simout.t, simout.x(:,1));
title('Horizontal Displacement','interpreter','latex');
xlabel('t [s]','interpreter','latex');
ylabel('$x$ [m]','interpreter','latex');

hf2=figure(2);
hf2.set('Name','Angular Displacement');
plot(simout.t, simout.x(:,2));
title('Angular Displacement','interpreter','latex');
xlabel('t [s]','interpreter','latex');
ylabel('$\theta$ [rad]','interpreter','latex');

hf3=figure(3);
hf3.set('Name','Horizontal Velocity');
plot(simout.t, simout.x(:,3));
title('Horizontal Velocity','interpreter','latex');
xlabel('t [s]','interpreter','latex');
ylabel('$\dot\theta$ [m]','interpreter','latex');

hf4=figure(4);
hf4.set('Name','Angular Velocity');
plot(simout.t, simout.x(:,4));
title('Angular Velocity','interpreter','latex');
xlabel('t [s]','interpreter','latex');
ylabel('$\dot x$ [rad]','interpreter','latex');

hf5=figure(5);
hf5.set('Name','Motor Torque');
plot(simout.t, simout.u);
title('Motor Torque','interpreter','latex');
xlabel('t [s]','interpreter','latex');
ylabel('$T$ [Nm]','interpreter','latex');

%% Animation
Wheel = p.rw*[cos(0:pi/30:2*pi)' sin(0:pi/30:2*pi)'
              -1                 0];
        
Body = [-.75*p.rw 0
         .75*p.rw 0
         .75*p.rw 5*p.rb
        -.75*p.rw 5*p.rb];


t_int = (0:0.18:max(simout.t))';
x_int = interp1(simout.t,simout.x(:,1),t_int);
th_int = interp1(simout.t,simout.x(:,2),t_int);

BaseRotated = zeros(size(Wheel,1),size(Wheel,2),length(t_int));
BodyRotated = zeros(size(Body,1),size(Body,2),length(t_int));

for n = 1:length(t_int)
    R1 = [cos(x_int(n)/p.rw) sin(x_int(n)/p.rw)
         -sin(x_int(n)/p.rw) cos(x_int(n)/p.rw)];
    R2 = [cos(th_int(n)) sin(th_int(n))
         -sin(th_int(n)) cos(th_int(n))];
    BaseRotated(:,:,n) = Wheel*R1' + [1 0]*x_int(n);
    BodyRotated(:,:,n) = Body*R2' + [1 0]*x_int(n);
end
hf6=figure(6);
set(hf6,'Name','Animation');
tic;
for n=1:length(t_int)
    figure(hf6);
    plot([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw],[-p.rw -p.rw],'linewidth',2);
    hold on;
    fill(BodyRotated(:,1,n),BodyRotated(:,2,n),'cyan','linewidth',2);
    fill(BaseRotated(:,1,n),BaseRotated(:,2,n),'blue','linewidth',2);
    hold off;
    axis equal;
    xlim([min(x_int(:,1))-15*p.rw max(x_int(:,1))+15*p.rw]);
    ylim(p.rb*[-2.5 5.5]);
    xlabel('X displacement');
    ylabel('Z displacement');
    title('Side View');
    grid on;
    drawnow;
end
toc;