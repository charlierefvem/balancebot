function [K,A,B] = getGain(p)
M = [   1   0   0           0
        0   1   0           0
        0   0   p.mx        p.mb*p.rb
        0   0   p.mb*p.rb   p.Ith ];

A = [   0   0           1   0
        0   0           0   1
        0   0           0   0
        0   p.mb*p.rb*g 0   0 ];

B = [   0
        0
       -2/p.rw
        2 ];

x1max = 0.1;
x2max = 1;
x3max = 0.01;
x4max = 0.01;
umax  = 0.1;


Q = diag(1./[x1max x2max x3max x4max].^2);
R = diag(1./[umax].^2);
    

Ts = 1/100;

A = M\A;
B = M\B;
figure(7)
pzmap(ss(A,B,eye(4),0))
title('Open Loop Poles')

%K = place(A,B,poles);
%K = lqr(A,B,Q,R);
K = lqrd(A,B,Q,R,Ts);

figure(8)
pzmap(ss(A-B*K,zeros(size(B)),eye(4),0))
title('Closed Loop Poles')

end

