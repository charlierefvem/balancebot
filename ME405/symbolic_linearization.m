syms mx mb rb Ith Ibxx Ibzz Ips g
syms th thd psid

M = [mx             mb*rb*cos(th)   0
     mb*rb*cos(th)  Ith             0
     0              0               mb*rb^2*sin(th)^2 + Ibxx*sin(th)^2 + Ibzz*cos(th)^2 + Ips]

simplify(1/det(M))

simplify(inv(M))

a = [mb*rb*sin(th)*thd^2
     mb*rb*sin(th)*g+mb*rb^2*sin(th)*cos(th)*psid^2+Ibxx*sin(th)*cos(th)*psid^2-Ibzz*sin(th)*cos(th)*psid^2
     -(2*mb*rb^2*sin(th)*cos(th)*thd + 2*Ibxx*sin(th)*cos(th)*thd - 2*Ibzz*sin(th)*cos(th)*thd)*psid]
 
simplify(M\a)