mb = 800e-3;
mw = 80e-3;
rb = 25e-3;
rw = 45e-3;
Iwyy = 2.5e-6;
Ibyy = 5.8e-3;
w = 120e-3;

Iwxx = 0.5*Iwyy;
Iwzz = 0.5*Iwyy;
Ibxx = Ibyy;
Ibzz = 1/4*Ibyy;

g = 9.81;
mx = mb + 2*mw + 2*(Iwyy/rw^2)
Ith = mb*rb^2 + Ibyy
Ips = mw*w^2/2 + Iwyy/rw^2*w^2/2 + 2*Iwzz


th = 0
thd = 0
psid = 0

M = [mx             mb*rb*cos(th)   0
     mb*rb*cos(th)  Ith             0
     0              0               mb*rb^2*sin(th)^2 + Ibxx*sin(th)^2 + Ibzz*cos(th)^2 + Ips]
M_ = [eye(3) zeros(3)
      zeros(3) M]
% x = [x th ps xd thd psd]
A_ = [ 0 0 0 1 0 0
       0 0 0 0 1 0
       0 0 0 0 0 1
       0 0 0 0 0 0
       0 mb*rb*g 0 0 0 0
       0 0 0 0 0 0 ]
B_ = [0 0
      0 0
      0 0
      -1/rw 1/rw
      1   -1
      -w/2/rw -w/2/rw]
A = M_\A_
B = M_\B_
C = eye(6)

ctrb(A,B)

obsv(A,C)